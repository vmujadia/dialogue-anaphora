This repo is for Hindi Dialogue anaphora resolution
## this mainly contains two dirs
    1. utils (whith following functionalites)
        1. includes module which can mark speaker information
        2. includes module which can mark subtopic of the given dialogue
    2. anaphora resolution module
        1. This module is used to mark anaphoric information for given discourse
