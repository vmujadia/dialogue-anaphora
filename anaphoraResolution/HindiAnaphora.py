import os
import argparse
import imp



parser = argparse.ArgumentParser()
parser.add_argument('-i', '--input', '-please specify the input file path')
parser.add_argument('-p', '--path', '-please specify the anaphoraresolver tool`s path')
    
args = parser.parse_args()

if args.input==None:
	print 'For help please use: python HindiAnaphora.py -h'
	exit()



modules=['ssfAPI_minimal','argparse','random','numpy','cPickle','sklearn']
for module in modules:
	try:
           imp.find_module(module)
        except ImportError:
           print 'Error'
	   print 'you don`t have following dependancy module :', module
	   if module=='sklearn':
		print 'for Ubuntu :'
		print '\t\t ::  sudo apt-get install python-numpy python-scipy python-matplotlib ipython ipython-notebook python-pandas python-sympy python-nose'
		print '\t\t :: install scikit-learn-0.15.2 given in HindiAnaphorav0.4/library folder'
		#print '\t\t ::  pip install -U scikit-learn'
		#print '\t\t ::  sudo apt-get install python-sklearn'

		print 'for fedora :'
		print '\t\t  :: sudo yum install numpy scipy python-matplotlib ipython python-pandas sympy python-nose'
		#print '\t\t ::  pip install -U scikit-learn'
		#print '\t\t  ::  sudo yum install python-scikit-learn'
		print '\t\t :: install scikit-learn-0.15.2 given in HindiAnaphorav0.4/library folder'
		print 'please go throw : '
		print '\t\t\t  http://scikit-learn.org/stable/install.html'		
	   exit()

modules=['ssfAPI_minimal','argparse','clientWordnet']
for module in modules:
    try:
        imp.find_module(module)
    except ImportError:
        print 'Error'
	print 'you don`t have following dependancy module :', module
	exit()


try:
	#os.system('perl '+args.path+'/testing/convertor-indic-1.4.9/convertor.pl -f=ssf -l=hin -s=wx -t=utf -i='+args.input+'>'+args.path+'/testing/'+'anaphoraTMP0.out')
	#print 'python '+args.path+'/testing/marksemprop.py -i '+args.path+'/testing/'+'anaphoraTMP0.out' +' > '+args.path+'/testing/'+'anaphoraTMP1.out'
	os.system('python '+args.path+'/testing/marksemprop.py -i '+args.input +' > '+args.path+'/testing/'+'anaphoraTMP1.out')
	os.system('python '+args.path+'/testing/anaphora_resolution_main.py -i '+args.path+'/testing/'+'anaphoraTMP1.out -p '+args.path+'/testing/' +' > '+args.path+'/testing/'+'anaphoraTMP2.out')
	os.system('python '+args.path+'/testing/featurecopyanaphora.py -i '+args.path+'/testing/'+'anaphoraTMP2.out')
	#os.system('perl '+args.path+'/testing/convertor-indic-1.4.9/convertor.pl -f=ssf -l=hin -s=utf -t=wx -i='+args.path+'/testing/'+'anaphoraTMP3.out')
	

	#os.system('perl '+args.path+'/testing/convertor-indic-1.4.9/convertor.pl -f=ssf -l=hin -s=utf -t=wx -i='+args.input+' > '+args.path+'/testing/'+'anaphoraTMP3.out')
	#os.system('python anaphora_resolution_main.py -i '+ 'anaphoraTMP0.out'+' -p '+args.path+' > anaphoraTMP1.out')
	#os.system('python featurecopyanaphora.py -i anaphoraTMP1.out > anaphoraTMP2.out')
	#os.system('perl '+'convertor-indic-1.4.9/convertor.pl -f=ssf -l=hin -s=utf -t=wx -i=utf.out')

	#'onsimple without semprop'
	#os.system('python marksemprop.py -i '+ args.input+' > anaphoraTMP0.out')
	#os.system('python anaphora_resolution_main.py -i '+ args.input+' -p '+args.path+' > anaphoraTMP1.out')
	#os.system('python featurecopyanaphora.py -i anaphoraTMP1.out')



	#first=anaphora_resolution_main('/home/vandan/workspace/anaphoraResolution/tools/data/vv.txt','preferencefiles/PREFERENCE')
	#first=anaphora_resolution_main(args.input,args.path+'/testing/preferencefiles/PREFERENCE',args.path+'/testing/model/my_dumped_classifier1.pkl')
except:
        print 'something went wrong please check the SSF file format' 


