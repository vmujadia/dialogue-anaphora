# -*- coding: utf-8 -*-
import codecs
import re




class firstPerson_resolution():
    
    def __init__(self, node):
        self.node=node
        self.chunk=self.node.upper
        self.tree=self.chunk.upper
        self.file=self.tree.upper
        self.verbrootforFP=[u'दोहरा',u'चाह',u'समझ',u'बता',u'बुला',u'कह']
        #self.FPResolve()
        
    def FPResolve(self):
        #print 'in first person resolution'
        flagsentence=False
        flagchunk=False
        # looking into same sentence 
        for a in reversed(self.file.nodeList):
            if (a.name==self.tree.name or flagsentence) and not self.chunk.isPronounresolved:
                flagsentence=True
                if int(a.name)-int(self.tree.name)==0:
                    #print 'sentence changed', a.name , self.tree.name
                    for c in reversed(a.nodeList):
                        #print c.name ,c.getAttribute('head')
                        if (c.name==self.chunk.name and self.chunk.isPronoun and  not self.chunk.isPronounresolved) or flagchunk:
                            flagchunk=True
                            if c.name==self.chunk.name:
                                continue
                            if c.morphroot != None:
                                if (c.morphroot in self.verbrootforFP) and (c.type=='VGF' or (c.type=='VGNN' and c.morphroot==u'कह')):
                                    ans=self.find_dep_in_stm(c,'k1', c.upper)
                                    if ans!=None and self.chunk.isPronoun and self.chunk!=ans:
                                        self.chunk.PrePronounrefstm=ans.upper
                                        self.chunk.PrePronounrefnode=ans
                                        self.chunk.isPronounresolved=True
                                        break
                                        #return self.chunk
                                        #print ans.getAttribute('head'), ans.name , ans.upper.name , 'VANDANMUJADIA',self.chunk.name,ans.upper.name
                    if not self.chunk.isPronounresolved:
                        ans=self.find_PRP_in_stm([u'मैं',u'हम'], self.chunk)
                        if ans!=None and self.chunk.isPronoun and self.chunk!=ans:
                            self.chunk.PrePronounrefstm=ans.upper
                            self.chunk.PrePronounrefnode=ans
                            self.chunk.isPronounresolved=True
                            break
                            #return self.chunk
                    '''
                    another rule not so use full
                    if given pronoun has pl and resolved entity has its r6 child then mark r6 as it`s referent 
                    '''
        #end looking into same sentence
        #start looking into -1 sentence 
        
                if int(self.tree.name)-int(a.name)==1:
                    #print 'sentence changed', a.name , self.tree.name
                    if not self.chunk.isPronounresolved:
                        ans=self.find_PRP_in_stm([u'मैं',u'हम'], self.chunk)
                        if ans!=None and self.chunk.isPronoun and self.chunk!=ans:
                            self.chunk.PrePronounrefstm=ans.upper
                            self.chunk.PrePronounrefnode=ans
                            self.chunk.isPronounresolved=True
                            break
                    if not self.chunk.isPronounresolved:
                        for c in reversed(a.nodeList):
                            if (self.chunk.isPronoun and not self.chunk.isPronounresolved):
                                if c.morphroot != None:
                                    if (c.morphroot in self.verbrootforFP) and (c.type=='VGF' or (c.type=='VGNN' and c.morphroot==u'कह')):
                                        ans=self.find_dep_in_stm_W_smp(c,'k1', c.upper,'h')
                                        if ans!=None and self.chunk.isPronoun and self.chunk!=ans:
                                            self.chunk.PrePronounrefstm=ans.upper
                                            self.chunk.PrePronounrefnode=ans
                                            self.chunk.isPronounresolved=True
                                            break
                    
                    if not self.chunk.isPronounresolved:
                        for c in reversed(a.nodeList):
                            if (self.chunk.isPronoun and not self.chunk.isPronounresolved):
                                if c.morphroot != None:
                                    if (c.morphroot in self.verbrootforFP):
                                        ans=self.find_dep_in_stm_W_smp(c,'k4', c.upper,'h')
                                        if ans!=None and self.chunk.isPronoun and self.chunk!=ans:
                                            self.chunk.PrePronounrefstm=ans.upper
                                            self.chunk.PrePronounrefnode=ans
                                            self.chunk.isPronounresolved=True
                                            break
        #end looking into -1 sentence
        
        #start looking into -2 sentence 
        
                if int(self.tree.name)-int(a.name)==2:
                    #print 'sentence changed', a.name , self.tree.name
                    if not self.chunk.isPronounresolved:
                        ans=self.find_PRP_in_stm([u'मैं',u'हम'], self.chunk)
                        if ans!=None and self.chunk.isPronoun and self.chunk!=ans:
                            self.chunk.PrePronounrefstm=ans.upper
                            self.chunk.PrePronounrefnode=ans
                            self.chunk.isPronounresolved=True
                            break
                    if not self.chunk.isPronounresolved:
                        for c in reversed(a.nodeList):
                            if (self.chunk.isPronoun and not self.chunk.isPronounresolved):
                                if c.morphroot != None:
                                    if (c.morphroot in self.verbrootforFP) and (c.type=='VGF' or (c.type=='VGNN' and c.morphroot==u'कह')):
                                        ans=self.find_dep_in_stm_W_smp(c,'k1', c.upper,'h')
                                        if ans!=None and self.chunk.isPronoun and self.chunk!=ans:
                                            self.chunk.PrePronounrefstm=ans.upper
                                            self.chunk.PrePronounrefnode=ans
                                            self.chunk.isPronounresolved=True
                                            break
                    if self.chunk.isPronounresolved:
                        if self.chunk.type=='VGNN':
                            ans=self.find_dep('r6', a)
                            if ans!=None and self.chunk.isPronoun and self.chunk!=ans:
                                self.chunk.PrePronounrefstm=ans.upper
                                self.chunk.PrePronounrefnode=ans
                                self.chunk.isPronounresolved=True
                                break
        #end looking into -2 sentence
        return self.chunk                        
                                
                                
    def find_dep(self,depL,stmL):
        ans=None
        for a in stmL.nodeList:
            if a.parentRelation==depL:
                ans=a
                break
        return ans
    def find_dep_in_stm(self,node,depL,stmL):
        ans=None
        for a in stmL.nodeList:
            if node.name==a.parent and a.parentRelation==depL:
                ans=a
                break
        return ans
    def find_dep_in_stm_W_smp(self,node,depL,stmL,semprop):
        ans=None
        for a in stmL.nodeList:
            if node.name==a.parent and a.parentRelation==depL and a.getAttribute('semprop')==semprop:
                ans=a
                break
        return ans
    
    def find_PRP_in_stm(self,PRP_root,chunk):
        ans=None
        flag=False
        for a in reversed(chunk.upper.nodeList):
            if flag:
                for b in a.nodeList:
                    if b.morphroot in PRP_root:
                        ans=a
            if a.name==chunk.name:
                flag=True
        return ans

class secondPerson_resolution():
    
    def __init__(self, node):
        self.node=node
        self.chunk=self.node.upper
        self.tree=self.chunk.upper
        self.file=self.tree.upper
        self.verbrootforFP=[u'दोहरा',u'चाह',u'समझ',u'बता',u'बुला',u'कह']
        #self.SPResolve()
        
    def SPResolve(self):
        #print 'in second person resolution'
        flagsentence=False
        flagchunk=False
        # looking into same sentence 
        for a in reversed(self.file.nodeList):
            if (a.name==self.tree.name or flagsentence) and not self.chunk.isPronounresolved:
                flagsentence=True
                if int(a.name)-int(self.tree.name)==0:
                    #print 'sentence changed', a.name , self.tree.name
                    for c in reversed(a.nodeList):
                        #print c.name ,c.getAttribute('head')
                        if (c.name==self.chunk.name and self.chunk.isPronoun and  not self.chunk.isPronounresolved) or flagchunk:
                            flagchunk=True
                            if c.name==self.chunk.name:
                                continue
                            if c.morphroot != None:
                                if (c.morphroot in self.verbrootforFP) and (c.type=='VGF' or (c.type=='VGNN' and c.morphroot==u'कह')):
                                    ans=self.find_dep_in_stm(c,'k4', c.upper)
                                    if ans!=None and self.chunk.isPronoun and self.chunk!=ans:
                                        self.chunk.PrePronounrefstm=ans.upper
                                        self.chunk.PrePronounrefnode=ans
                                        self.chunk.isPronounresolved=True
                                        break
                                        #return self.chunk
                                        #print ans.getAttribute('head'), ans.name , ans.upper.name , 'VANDANMUJADIA',self.chunk.name,ans.upper.name
                    '''
                    another rule not so use full
                    if given pronoun has pl and resolved entity has its r6 child then mark r6 as it`s referent 
                    '''
        #end looking into same sentence
        #start looking into -1 sentence 
        
                if int(self.tree.name)-int(a.name)==1:
                    #print 'sentence changed', a.name , self.tree.name
                    if not self.chunk.isPronounresolved:
                        for c in reversed(a.nodeList):
                            if (self.chunk.isPronoun and not self.chunk.isPronounresolved):
                                if c.morphroot != None:
                                    if (c.morphroot in self.verbrootforFP) and (c.type=='VGF' or (c.type=='VGNN' and c.morphroot==u'कह')):
                                        ans=self.find_dep_in_stm(c,'rt', c.upper)
                                        if ans!=None and self.chunk.isPronoun and self.chunk!=ans:
                                            self.chunk.PrePronounrefstm=ans.upper
                                            self.chunk.PrePronounrefnode=ans
                                            self.chunk.isPronounresolved=True
                                            break
                    if not self.chunk.isPronounresolved:
                        for c in reversed(a.nodeList):
                            if (self.chunk.isPronoun and not self.chunk.isPronounresolved):
                                if c.morphroot != None:
                                    if (c.morphroot in self.verbrootforFP):
                                        ans=self.find_dep_in_stm_W_smp(c,'r6', c.upper,c.getAttribute('semprop'))
                                        if ans!=None and self.chunk.isPronoun and self.chunk!=ans:
                                            self.chunk.PrePronounrefstm=ans.upper
                                            self.chunk.PrePronounrefnode=ans
                                            self.chunk.isPronounresolved=True
                                            break
        #end looking into -1 sentence
        
        #start looking into -2 sentence 
        
                if int(self.tree.name)-int(a.name)==2:
                    #print 'sentence changed', a.name , self.tree.name
                    if not self.chunk.isPronounresolved:
                        for c in reversed(a.nodeList):
                            if (self.chunk.isPronoun and not self.chunk.isPronounresolved):
                                if c.morphroot != None:
                                    if (c.morphroot in self.verbrootforFP) and (c.type=='VGF' or (c.type=='VGNN' and c.morphroot==u'कह')):
                                        ans=self.find_dep_in_stm(c,'k1', c.upper)
                                        if ans!=None and self.chunk.isPronoun and self.chunk!=ans:
                                            self.chunk.PrePronounrefstm=ans.upper
                                            self.chunk.PrePronounrefnode=ans
                                            self.chunk.isPronounresolved=True
                                            break
        #end looking into -2 sentence
        return self.chunk                        
                                
                                
    def find_dep(self,depL,stmL):
        ans=None
        for a in stmL.nodeList:
            if a.parentRelation==depL:
                ans=a
                break
        return ans
    def find_dep_in_stm(self,node,depL,stmL):
        ans=None
        for a in stmL.nodeList:
            if node.name==a.parent and a.parentRelation==depL:
                ans=a
                break
        return ans
    def find_dep_in_stm_W_smp(self,node,depL,stmL,semprop):
        ans=None
        for a in stmL.nodeList:
            if node.name==a.parent and a.parentRelation==depL and a.getAttribute('semprop')==semprop:
                ans=a
                break
        return ans
    
    def find_PRP_in_stm(self,PRP_root,chunk):
        ans=None
        flag=False
        for a in reversed(chunk.upper.nodeList):
            if flag:
                for b in a.nodeList:
                    if b.morphroot in PRP_root:
                        ans=a
            if a.name==chunk.name:
                flag=True
        return ans
    
class locative_resolution():
    
    def __init__(self, node):
        self.node=node
        self.chunk=self.node.upper
        self.tree=self.chunk.upper
        self.file=self.tree.upper
        #self.LResolve()
        
    def LResolve(self):
        #print 'in locative resolution'
        flagsentence=False
        flagchunk=False
        # looking into same sentence 
        for a in reversed(self.file.nodeList):
            if (a.name==self.tree.name or flagsentence) and not self.chunk.isPronounresolved:
                flagsentence=True
                if int(a.name)-int(self.tree.name)==0:
                    #print 'sentence changed', a.name , self.tree.name
                    for c in reversed(a.nodeList):
                        if (c.name==self.chunk.name and self.chunk.isPronoun and  not self.chunk.isPronounresolved) or flagchunk:
                            flagchunk=True
                            if c.name==self.chunk.name:
                                continue
                            ans=None
                            if (c.parentRelation=='k7p' or c.parentRelation=='k2p') and c.getAttribute('semprop')==self.chunk.getAttribute('semprop'):
                                ans=c
                                if ans!=None and self.chunk.isPronoun and self.chunk!=ans:
                                    self.chunk.PrePronounrefstm=ans.upper
                                    self.chunk.PrePronounrefnode=ans
                                    self.chunk.isPronounresolved=True
                                    break
                    flagchunk=False
                    for c in reversed(a.nodeList):
                        if (c.name==self.chunk.name and self.chunk.isPronoun and  not self.chunk.isPronounresolved) or flagchunk:
                            flagchunk=True
                            if c.name==self.chunk.name:
                                continue
                            ans=None
                            if (c.morphroot==u'यहां') and c.getAttribute('semprop')=='rest':
                                ans=c
                                if ans!=None and self.chunk.isPronoun and self.chunk!=ans:
                                    self.chunk.PrePronounrefstm=ans.upper
                                    self.chunk.PrePronounrefnode=ans
                                    self.chunk.isPronounresolved=True
                                    break
                    flagchunk=False
                    for c in reversed(a.nodeList):
                        if (c.name==self.chunk.name and self.chunk.isPronoun and  not self.chunk.isPronounresolved) or flagchunk:
                            flagchunk=True
                            if c.name==self.chunk.name:
                                continue
                            ans=None
                            if (c.morphroot==u'वहाँ') and c.getAttribute('semprop')=='rest':
                                ans=c
                                if ans!=None and self.chunk.isPronoun and self.chunk!=ans:
                                    self.chunk.PrePronounrefstm=ans.upper
                                    self.chunk.PrePronounrefnode=ans
                                    self.chunk.isPronounresolved=True
                                    break
        #end looking into same sentence
        #start looking into -1 sentence 
        
                if int(self.tree.name)-int(a.name)==1:
                    #print 'sentence changed', a.name , self.tree.name
                    for c in reversed(a.nodeList):
                        if (c.name==self.chunk.name and self.chunk.isPronoun and  not self.chunk.isPronounresolved) or flagchunk:
                            flagchunk=True
                            if c.name==self.chunk.name:
                                continue
                            ans=None
                            if (c.parentRelation=='k7p' or c.parentRelation=='k2p') and c.getAttribute('semprop')=='rest':
                                ans=c
                                if ans!=None and self.chunk.isPronoun and self.chunk!=ans:
                                    self.chunk.PrePronounrefstm=ans.upper
                                    self.chunk.PrePronounrefnode=ans
                                    self.chunk.isPronounresolved=True
                                    break
                    for c in reversed(a.nodeList):
                        if (c.name==self.chunk.name and self.chunk.isPronoun and  not self.chunk.isPronounresolved) or flagchunk:
                            flagchunk=True
                            if c.name==self.chunk.name:
                                continue
                            ans=None
                            if (c.morphroot==u'वहाँ') and c.getAttribute('semprop')=='rest':
                                ans=c
                                if ans!=None and self.chunk.isPronoun and self.chunk!=ans:
                                    self.chunk.PrePronounrefstm=ans.upper
                                    self.chunk.PrePronounrefnode=ans
                                    self.chunk.isPronounresolved=True
                                    break
                    for c in reversed(a.nodeList):
                        if (c.name==self.chunk.name and self.chunk.isPronoun and  not self.chunk.isPronounresolved) or flagchunk:
                            flagchunk=True
                            if c.name==self.chunk.name:
                                continue
                            ans=None
                            if (c.morphroot==u'यहां') and c.getAttribute('semprop')=='rest':
                                ans=c
                                if ans!=None and self.chunk.isPronoun and self.chunk!=ans:
                                    self.chunk.PrePronounrefstm=ans.upper
                                    self.chunk.PrePronounrefnode=ans
                                    self.chunk.isPronounresolved=True
                                    break
        #end looking into -1 sentence
        #Start between
                for c in reversed(a.nodeList):
                    if (c.name==self.chunk.name and self.chunk.isPronoun and  not self.chunk.isPronounresolved) or flagchunk:
                        flagchunk=True
                        if c.name==self.chunk.name:
                            continue
                        ans=None
                        if (c.parentRelation=='r6-k2') and c.getAttribute('semprop')=='rest':
                            ans=c
                            if ans!=None and self.chunk.isPronoun and self.chunk!=ans:
                                self.chunk.PrePronounrefstm=ans.upper
                                self.chunk.PrePronounrefnode=ans
                                self.chunk.isPronounresolved=True
                                break
        #End between
        #start looking into -2 sentence 
        
                if int(self.tree.name)-int(a.name)==2:
                    #print 'sentence changed', a.name , self.tree.name
                    for c in reversed(a.nodeList):
                        if (c.name==self.chunk.name and self.chunk.isPronoun and  not self.chunk.isPronounresolved) or flagchunk:
                            flagchunk=True
                            if c.name==self.chunk.name:
                                continue
                            ans=None
                            if (c.parentRelation=='k7p' or c.parentRelation=='k2p') and c.getAttribute('semprop')=='rest':
                                ans=c
                                if ans!=None and self.chunk.isPronoun and self.chunk!=ans:
                                    self.chunk.PrePronounrefstm=ans.upper
                                    self.chunk.PrePronounrefnode=ans
                                    self.chunk.isPronounresolved=True
                                    break
        #end looking into -2 sentence
        #start looking into -3 sentence 
        
                if int(self.tree.name)-int(a.name)==3:
                    #print 'sentence changed', a.name , self.tree.name
                    for c in reversed(a.nodeList):
                        if (c.name==self.chunk.name and self.chunk.isPronoun and  not self.chunk.isPronounresolved) or flagchunk:
                            flagchunk=True
                            if c.name==self.chunk.name:
                                continue
                            ans=None
                            if (c.parentRelation=='k7p' or c.parentRelation=='k2p') and c.getAttribute('semprop')=='rest':
                                ans=c
                                if ans!=None and self.chunk.isPronoun and self.chunk!=ans:
                                    self.chunk.PrePronounrefstm=ans.upper
                                    self.chunk.PrePronounrefnode=ans
                                    self.chunk.isPronounresolved=True
                                    break
        #end looking into -3 sentence
        #exctra work
                for c in reversed(a.nodeList):
                    if (c.name==self.chunk.name and self.chunk.isPronoun and  not self.chunk.isPronounresolved) or flagchunk:
                        flagchunk=True
                        if c.name==self.chunk.name:
                            continue
                        ans=None
                        if c.getAttribute('enamex_type')=='location' and c.getAttribute('semprop')=='rest':
                            ans=c
                            if ans!=None and self.chunk.isPronoun and self.chunk!=ans:
                                self.chunk.PrePronounrefstm=ans.upper
                                self.chunk.PrePronounrefnode=ans
                                self.chunk.isPronounresolved=True
                                break
                for c in reversed(a.nodeList):
                    if (c.name==self.chunk.name and self.chunk.isPronoun and  not self.chunk.isPronounresolved) or flagchunk:
                        flagchunk=True
                        if c.name==self.chunk.name:
                            continue
                        ans=None
                        if (c.parentRelation=='k7') and c.getAttribute('semprop')=='rest':
                            ans=c
                            if ans!=None and self.chunk.isPronoun and self.chunk!=ans:
                                self.chunk.PrePronounrefstm=ans.upper
                                self.chunk.PrePronounrefnode=ans
                                self.chunk.isPronounresolved=True
                                break
        #end of exctra work
        
        return self.chunk                  
                                
                                
    def find_dep(self,depL,stmL):
        ans=None
        for a in stmL.nodeList:
            if a.parentRelation==depL:
                ans=a
                break
        return ans
    def find_dep_in_stm(self,node,depL,stmL):
        ans=None
        for a in stmL.nodeList:
            if node.name==a.parent and a.parentRelation==depL:
                ans=a
                break
        return ans
    def find_dep_in_stm_W_smp(self,node,depL,stmL,semprop):
        ans=None
        for a in stmL.nodeList:
            if node.name==a.parent and a.parentRelation==depL and a.getAttribute('semprop')==semprop:
                ans=a
                break
        return ans
    
    def find_PRP_in_stm(self,PRP_root,chunk):
        ans=None
        flag=False
        for a in reversed(chunk.upper.nodeList):
            if flag:
                for b in a.nodeList:
                    if b.morphroot in PRP_root:
                        ans=a
            if a.name==chunk.name:
                flag=True
        return ans

class relative_resolution():
    
    def __init__(self, node,chunk):
        self.node=node
        self.chunk=chunk
        self.tree=chunk.upper
        self.file=self.tree.upper
        #self.RResolve()
        
    def RResolve(self):
        #print 'in relative resolution'
        flagsentence=False
        flagchunk=False
        # looking into same sentence 
        for a in reversed(self.file.nodeList):
            if (a.name==self.tree.name or flagsentence) and not self.chunk.isPronounresolved:
                flagsentence=True
                if int(a.name)-int(self.tree.name)==0:
                    #print 'sentence changed', a.name , self.tree.name
                    #start of JO0
                    for c in reversed(a.nodeList):
                        if (c.name==self.chunk.name and self.chunk.isPronoun and  not self.chunk.isPronounresolved) or flagchunk:
                            flagchunk=True
                            if c.name==self.chunk.name:
                                continue
                            for d in reversed(c.nodeList):
                                if not (d.type=='DEM' and d.morphroot==u'यह' and d.upper.parentRelation=='k1' and d.upper.getAttribute('semprop')=='rest') or (d.type=='DEM' and d.morphroot==u'यह' and (d.upper.parentRelation=='k7t' or d.upper.parentRelation=='k7') and d.upper.getAttribute('semprop')=='rest') or (d.type=='DEM' and d.morphroot==u'वह' and d.upper.parentRelation=='k1' and d.upper.getAttribute('semprop')=='rest')or (d.type=='DEM' and d.morphroot==u'यह' and d.upper.parentRelation=='rt' and d.upper.getAttribute('semprop')=='rest'):
                                    if self.chunk.getAttribute('semprop')==d.upper.getAttribute('semprop'):
                                        ans=c
                                        if ans!=None and self.chunk.isPronoun and self.chunk!=ans:
                                            self.chunk.PrePronounrefstm=ans.upper
                                            self.chunk.PrePronounrefnode=ans
                                            self.chunk.isPronounresolved=True
                                            break
                    # end of JOO
                    flagchunk=False
                    for c in reversed(a.nodeList):
                        if (c.name==self.chunk.name and self.chunk.isPronoun and  not self.chunk.isPronounresolved) or flagchunk:
                            flagchunk=True
                            if c.name==self.chunk.name:
                                continue
                            for d in reversed(c.nodeList):
                                if d.type=='VM':
                                    ans=self.find_dep_in_stm_W_smp(c,'k1u', c.upper,self.chunk.getAttribute('semprop'))
                                    if ans.isPronoun:
                                        if ans!=None and self.chunk.isPronoun and self.chunk!=ans:
                                            self.chunk.PrePronounrefstm=ans.upper
                                            self.chunk.PrePronounrefnode=ans
                                            self.chunk.isPronounresolved=True
                                            break
                    # Start of JoO
                    flagchunk=False
                    for c in reversed(a.nodeList):
                        if (c.name==self.chunk.name and self.chunk.isPronoun and  not self.chunk.isPronounresolved) or flagchunk:
                            flagchunk=True
                            if c.name==self.chunk.name:
                                continue
                            if c.parentRelation=='pof' and re.search(r'VGF.',c.parent,re.M|re.I) and self.chunk.getAttribute('semprop')==c.getAttribute('semprop') and d.morphPOS=='n':
                                        if ans!=None and self.chunk.isPronoun and self.chunk!=ans:
                                            self.chunk.PrePronounrefstm=ans.upper
                                            self.chunk.PrePronounrefnode=ans
                                            self.chunk.isPronounresolved=True
                                            break 
                    flagchunk=False
                    for c in reversed(a.nodeList):
                        if (c.name==self.chunk.name and self.chunk.isPronoun and  not self.chunk.isPronounresolved) or flagchunk:
                            flagchunk=True
                            if c.name==self.chunk.name:
                                continue
                            for d in reversed(c.nodeList):
                                if d.type=='VM':
                                    ans=self.find_dep_in_stm_W_smp(c,'k1s', c.upper,self.chunk.getAttribute('semprop'))
                                    if True:
                                        if ans!=None and self.chunk.isPronoun and self.chunk!=ans:
                                            self.chunk.PrePronounrefstm=ans.upper
                                            self.chunk.PrePronounrefnode=ans
                                            self.chunk.isPronounresolved=True
                                            break
                    temp_t1=False
                    flagchunk=False
                    for c in reversed(a.nodeList):
                        if (c.name==self.chunk.name and self.chunk.isPronoun and  not self.chunk.isPronounresolved) or flagchunk:
                                flagchunk=True
                                if c.name==self.chunk.name:
                                    continue
                                if (c.parentRelation=='k2u' or c.parentRelation=='k1' or c.parentRelation=='k2' or c.parentRelation=='k1s') and self.chunk.getAttribute('semprop')==c.getAttribute('semprop'):
                                        ans=c
                                        temp_t1=True
                    flagchunk=False
                    if temp_t1:
                        for c in reversed(a.nodeList):
                            if c.parent==ans.getAttribute('name') and c.parentRelation=='nmod' and self.chunk.getAttribute('semprop')==c.getAttribute('semprop')and c.morphroot==u'वह':
                                ans=c
                                if ans!=None and self.chunk.isPronoun and self.chunk!=ans:
                                            self.chunk.PrePronounrefstm=ans.upper
                                            self.chunk.PrePronounrefnode=ans
                                            self.chunk.isPronounresolved=True
                                            temp_t1=False
                                            break
                    flagchunk=False
                    if self.chunk.getAttribute('semprop')=='h':
                        for c in reversed(a.nodeList):
                            if (c.name==self.chunk.name and self.chunk.isPronoun and  not self.chunk.isPronounresolved) or flagchunk:
                                flagchunk=True
                                if c.name==self.chunk.name:
                                    continue
                                if (c.parentRelation=='r6') and self.chunk.getAttribute('semprop')==d.upper.getAttribute('semprop'):
                                        ans=c
                                        if ans!=None and self.chunk.isPronoun and self.chunk!=ans:
                                            self.chunk.PrePronounrefstm=ans.upper
                                            self.chunk.PrePronounrefnode=ans
                                            self.chunk.isPronounresolved=True
                                            break
                    # end of JO
                    flagchunk=False
                    for c in reversed(a.nodeList):
                            if (c.name==self.chunk.name and self.chunk.isPronoun and  not self.chunk.isPronounresolved) or flagchunk:
                                flagchunk=True
                                if c.name==self.chunk.name:
                                    continue
                                if (c.parentRelation=='pof') and re.search(r'VGF.',c.parent,re.M|re.I)and self.chunk.getAttribute('semprop')==c.getAttribute('semprop') and c.morphPOS=='n':
                                        ans=c
                                        if ans!=None and self.chunk.isPronoun and self.chunk!=ans:
                                            self.chunk.PrePronounrefstm=ans.upper
                                            self.chunk.PrePronounrefnode=ans
                                            self.chunk.isPronounresolved=True
                                            break
                    flagchunk=False
                    for c in reversed(a.nodeList):
                            if (c.name==self.chunk.name and self.chunk.isPronoun and  not self.chunk.isPronounresolved) or flagchunk:
                                flagchunk=True
                                if c.name==self.chunk.name:
                                    continue
                                if (c.parentRelation=='vmod') and re.search(r'VGF',c.parent,re.M|re.I)and self.chunk.getAttribute('semprop')==c.getAttribute('semprop') and c.morphPOS=='n':
                                        ans=c
                                        if ans!=None and self.chunk.isPronoun and self.chunk!=ans:
                                            self.chunk.PrePronounrefstm=ans.upper
                                            self.chunk.PrePronounrefnode=ans
                                            self.chunk.isPronounresolved=True
                                            break
                    PRP_HValue=None
                    flagchunk=False
                    for e in (self.chunk.nodeList):
                        if e.type=='PRP':
                            PRP_HValue=e.lex
                    
                    chunk_no_count=0
                    flagchunk=False
                    for c in reversed(a.nodeList):
                            if (c.name==self.chunk.name and self.chunk.isPronoun and  not self.chunk.isPronounresolved) or flagchunk:
                                flagchunk=True
                                if c.name==self.chunk.name:
                                    chunk_no_count=0
                                    continue
                                else:
                                    chunk_no_count=chunk_no_count+1
                                    for d in c.nodeList:
                                        if d.lex=='':
                                            continue
                                    if chunk_no_count>7:
                                        break
                                    if PRP_HValue =='' and (c.parentRelation=='r6') and self.chunk.number==c.number and self.chunk.getAttribute('semprop')==c.getAttribute('semprop') and c.morphPOS=='n':
                                        ans=c
                                        if ans!=None and self.chunk.isPronoun and self.chunk!=ans:
                                            self.chunk.PrePronounrefstm=ans.upper
                                            self.chunk.PrePronounrefnode=ans
                                            self.chunk.isPronounresolved=True
                                            break
                    flagchunk=False
                    for c in reversed(a.nodeList):
                        if (c.name==self.chunk.name and self.chunk.isPronoun and  not self.chunk.isPronounresolved) or flagchunk:
                            flagchunk=True
                            if c.name==self.chunk.name:
                                continue
                            for d in reversed(c.nodeList):
                                if d.type=='VM':
                                    ans=self.find_dep_in_stm_W_smp_emax_num(c,'k1', c.upper,self.chunk.getAttribute('semprop'),'person',self.chunk.number)
                                    if True:
                                        if ans!=None and self.chunk.isPronoun and self.chunk!=ans:
                                            self.chunk.PrePronounrefstm=ans.upper
                                            self.chunk.PrePronounrefnode=ans
                                            self.chunk.isPronounresolved=True
                                            break
                    flagchunk=False
                    for c in reversed(a.nodeList):
                        if (c.name==self.chunk.name and self.chunk.isPronoun and  not self.chunk.isPronounresolved) or flagchunk:
                            flagchunk=True
                            if c.name==self.chunk.name:
                                continue
                            for d in reversed(c.nodeList):
                                if d.type=='VM':
                                    ans=self.find_dep_in_stm_W_smp(c,'k1', c.upper,self.chunk.getAttribute('semprop'))
                                    if True:
                                        if ans!=None and self.chunk.isPronoun and self.chunk!=ans:
                                            self.chunk.PrePronounrefstm=ans.upper
                                            self.chunk.PrePronounrefnode=ans
                                            self.chunk.isPronounresolved=True
                                            break
                    flagchunk=False
                    for c in reversed(a.nodeList):
                        if (c.name==self.chunk.name and self.chunk.isPronoun and  not self.chunk.isPronounresolved) or flagchunk:
                            flagchunk=True
                            if c.name==self.chunk.name:
                                continue
                            for d in reversed(c.nodeList):
                                if d.type=='VM':
                                    ans=self.find_dep_in_stm_W_smp(c,'r6', c.upper,self.chunk.getAttribute('semprop'))
                                    if True:
                                        if ans!=None and self.chunk.isPronoun and self.chunk!=ans:
                                            self.chunk.PrePronounrefstm=ans.upper
                                            self.chunk.PrePronounrefnode=ans
                                            self.chunk.isPronounresolved=True
                                            break
                    flagchunk=False
                    for c in reversed(a.nodeList):
                        if (c.name==self.chunk.name and self.chunk.isPronoun and  not self.chunk.isPronounresolved) or flagchunk:
                            flagchunk=True
                            if c.name==self.chunk.name:
                                continue
                            for d in reversed(c.nodeList):
                                if d.type=='VM':
                                    ans=self.find_dep_in_stm_W_smp(c,'k2', c.upper,self.chunk.getAttribute('semprop'))
                                    if True:
                                        if ans!=None and self.chunk.isPronoun and self.chunk!=ans:
                                            self.chunk.PrePronounrefstm=ans.upper
                                            self.chunk.PrePronounrefnode=ans
                                            self.chunk.isPronounresolved=True
                                            break
                    flagchunk=False
                    for c in reversed(a.nodeList):
                        if (c.name==self.chunk.name and self.chunk.isPronoun and  not self.chunk.isPronounresolved) or flagchunk:
                            flagchunk=True
                            if c.name==self.chunk.name:
                                continue
                                if (c.parentRelation=='rt') and self.chunk.getAttribute('semprop')==c.getAttribute('semprop'):    
                                    if True:
                                        if ans!=None and self.chunk.isPronoun and self.chunk!=ans:
                                            self.chunk.PrePronounrefstm=ans.upper
                                            self.chunk.PrePronounrefnode=ans
                                            self.chunk.isPronounresolved=True
                                            break
                    flagchunk=False
                    for c in reversed(a.nodeList):
                        if (c.name==self.chunk.name and self.chunk.isPronoun and  not self.chunk.isPronounresolved) or flagchunk:
                            flagchunk=True
                            if c.name==self.chunk.name:
                                continue
                                if (c.parentRelation=='ccof') and self.chunk.getAttribute('semprop')==c.getAttribute('semprop'):    
                                    if True:
                                        if ans!=None and self.chunk.isPronoun and self.chunk!=ans:
                                            self.chunk.PrePronounrefstm=ans.upper
                                            self.chunk.PrePronounrefnode=ans
                                            self.chunk.isPronounresolved=True
                                            break
                    flagchunk=False
                    for c in reversed(a.nodeList):
                        if (c.name==self.chunk.name and self.chunk.isPronoun and  not self.chunk.isPronounresolved) or flagchunk:
                            flagchunk=True
                            if c.name==self.chunk.name:
                                continue
                                if (c.parentRelation=='r6-k2') and self.chunk.getAttribute('semprop')==c.getAttribute('semprop'):    
                                    if True:
                                        if ans!=None and self.chunk.isPronoun and self.chunk!=ans:
                                            self.chunk.PrePronounrefstm=ans.upper
                                            self.chunk.PrePronounrefnode=ans
                                            self.chunk.isPronounresolved=True
                                            break
                    flagchunk=False
                    for c in reversed(a.nodeList):
                        if (c.name==self.chunk.name and self.chunk.isPronoun and  not self.chunk.isPronounresolved) or flagchunk:
                            flagchunk=True
                            if c.name==self.chunk.name:
                                continue
                                if (c.parentRelation=='r6') and self.chunk.getAttribute('semprop')==c.getAttribute('semprop'):    
                                    if True:
                                        if ans!=None and self.chunk.isPronoun and self.chunk!=ans:
                                            self.chunk.PrePronounrefstm=ans.upper
                                            self.chunk.PrePronounrefnode=ans
                                            self.chunk.isPronounresolved=True
                                            break
                    
                    flagchunk=False
                    for c in reversed(a.nodeList):
                        if (c.name==self.chunk.name and self.chunk.isPronoun and  not self.chunk.isPronounresolved) or flagchunk:
                            flagchunk=True
                            if c.name==self.chunk.name:
                                continue
                                if (c.parentRelation=='ccof') and self.chunk.getAttribute('semprop')==c.getAttribute('semprop'):    
                                    if True:
                                        if ans!=None and self.chunk.isPronoun and self.chunk!=ans:
                                            self.chunk.PrePronounrefstm=ans.upper
                                            self.chunk.PrePronounrefnode=ans
                                            self.chunk.isPronounresolved=True
                                            break
                #end looking into same sentence
                #start looking into -1 sentence
                if int(self.tree.name)-int(a.name)==1:
                    for c in reversed(a.nodeList):
                        if (c.name==self.chunk.name and self.chunk.isPronoun and  not self.chunk.isPronounresolved) or flagchunk:
                            flagchunk=True
                            if c.name==self.chunk.name:
                                continue
                                if (c.parentRelation=='k2') and self.chunk.getAttribute('semprop')==c.getAttribute('semprop') and self.chunk.number==c.number and self.chunk.person==c.person:    
                                    if True:
                                        if ans!=None and self.chunk.isPronoun and self.chunk!=ans:
                                            self.chunk.PrePronounrefstm=ans.upper
                                            self.chunk.PrePronounrefnode=ans
                                            self.chunk.isPronounresolved=True
                                            break
                    
                    for c in reversed(a.nodeList):
                        if (c.name==self.chunk.name and self.chunk.isPronoun and  not self.chunk.isPronounresolved) or flagchunk:
                            flagchunk=True
                            if c.name==self.chunk.name:
                                continue
                            for d in reversed(c.nodeList):
                                if d.type=='VM':
                                    ans=self.find_dep_in_stm_W_smp_per(c,'k1', c.upper,self.chunk.getAttribute('semprop'),self.chunk.person)
                                    if True:
                                        if ans!=None and self.chunk.isPronoun and self.chunk!=ans:
                                            self.chunk.PrePronounrefstm=ans.upper
                                            self.chunk.PrePronounrefnode=ans
                                            self.chunk.isPronounresolved=True
                                            break
                    
                    for c in reversed(a.nodeList):
                        if (c.name==self.chunk.name and self.chunk.isPronoun and  not self.chunk.isPronounresolved) or flagchunk:
                            flagchunk=True
                            if c.name==self.chunk.name:
                                continue
                            for d in reversed(c.nodeList):
                                if d.type=='VM':
                                    ans=self.find_dep_in_stm_W_smp_per(c,'r6', c.upper,self.chunk.getAttribute('semprop'),self.chunk.person)
                                    if True:
                                        if ans!=None and self.chunk.isPronoun and self.chunk!=ans:
                                            self.chunk.PrePronounrefstm=ans.upper
                                            self.chunk.PrePronounrefnode=ans
                                            self.chunk.isPronounresolved=True
                                            break
                    
                    for c in reversed(a.nodeList):
                        if (c.name==self.chunk.name and self.chunk.isPronoun and  not self.chunk.isPronounresolved) or flagchunk:
                            flagchunk=True
                            if c.name==self.chunk.name:
                                continue
                            for d in reversed(c.nodeList):
                                if d.type=='VM':
                                    ans=self.find_dep_in_stm_W_smp_per(c,'k2', c.upper,self.chunk.getAttribute('semprop'),self.chunk.person)
                                    if True:
                                        if ans!=None and self.chunk.isPronoun and self.chunk!=ans:
                                            self.chunk.PrePronounrefstm=ans.upper
                                            self.chunk.PrePronounrefnode=ans
                                            self.chunk.isPronounresolved=True
                                            break
                    
                    for c in reversed(a.nodeList):
                        if (c.name==self.chunk.name and self.chunk.isPronoun and  not self.chunk.isPronounresolved) or flagchunk:
                            flagchunk=True
                            if c.name==self.chunk.name:
                                continue
                            for d in reversed(c.nodeList):
                                if d.type=='VM':
                                    ans=self.find_dep_in_stm_W_smp_per(c,'k4', c.upper,self.chunk.getAttribute('semprop'),self.chunk.person)
                                    if True:
                                        if ans!=None and self.chunk.isPronoun and self.chunk!=ans:
                                            self.chunk.PrePronounrefstm=ans.upper
                                            self.chunk.PrePronounrefnode=ans
                                            self.chunk.isPronounresolved=True
                                            break
                    
                    for c in reversed(a.nodeList):
                        if (c.name==self.chunk.name and self.chunk.isPronoun and  not self.chunk.isPronounresolved) or flagchunk:
                            flagchunk=True
                            if c.name==self.chunk.name:
                                continue
                            for d in reversed(c.nodeList):
                                if d.type=='VM':
                                    ans=self.find_dep_in_stm_W_smp_per(c,'ccof', c.upper,self.chunk.getAttribute('semprop'),self.chunk.person)
                                    if True:
                                        if ans!=None and self.chunk.isPronoun and self.chunk!=ans:
                                            self.chunk.PrePronounrefstm=ans.upper
                                            self.chunk.PrePronounrefnode=ans
                                            self.chunk.isPronounresolved=True
                                            break
                    
                    for c in reversed(a.nodeList):
                        if (c.name==self.chunk.name and self.chunk.isPronoun and  not self.chunk.isPronounresolved) or flagchunk:
                            flagchunk=True
                            if c.name==self.chunk.name:
                                continue
                            for d in reversed(c.nodeList):
                                if d.type=='VM':
                                    if c.type=='VGF':
                                        ans=self.find_dep_in_stm_W_smp_per(c,'k1', c.upper,self.chunk.getAttribute('semprop'),self.chunk.person)
                                        if ans!=None and self.chunk.isPronoun and self.chunk!=ans:
                                            self.chunk.PrePronounrefstm=ans.upper
                                            self.chunk.PrePronounrefnode=ans
                                            self.chunk.isPronounresolved=True
                                            break
                                        
                                        
                    for c in reversed(a.nodeList):
                        if (c.name==self.chunk.name and self.chunk.isPronoun and  not self.chunk.isPronounresolved) or flagchunk:
                            flagchunk=True
                            if c.name==self.chunk.name:
                                continue
                                if (c.parentRelation=='r6-k2') and self.chunk.getAttribute('semprop')==c.getAttribute('semprop'):    
                                    if True:
                                        if ans!=None and self.chunk.isPronoun and self.chunk!=ans:
                                            self.chunk.PrePronounrefstm=ans.upper
                                            self.chunk.PrePronounrefnode=ans
                                            self.chunk.isPronounresolved=True
                                            break
                #end looking into -1 sentence
                #start looking into -2 sentence
                if int(self.tree.name)-int(a.name)==2:
                    for c in reversed(a.nodeList):
                        if (c.name==self.chunk.name and self.chunk.isPronoun and  not self.chunk.isPronounresolved) or flagchunk:
                            flagchunk=True
                            if c.name==self.chunk.name:
                                continue
                            for d in reversed(c.nodeList):
                                if d.type=='VM':
                                    ans=self.find_dep_in_stm_W_smp_per(c,'k1', c.upper,self.chunk.getAttribute('semprop'),self.chunk.person)
                                    if True:
                                        if ans!=None and self.chunk.isPronoun and self.chunk!=ans:
                                            self.chunk.PrePronounrefstm=ans.upper
                                            self.chunk.PrePronounrefnode=ans
                                            self.chunk.isPronounresolved=True
                                            break
                    for c in reversed(a.nodeList):
                        if (c.name==self.chunk.name and self.chunk.isPronoun and  not self.chunk.isPronounresolved) or flagchunk:
                            flagchunk=True
                            if c.name==self.chunk.name:
                                continue
                            for d in reversed(c.nodeList):
                                if d.type=='VM':
                                    ans=self.find_dep_in_stm_W_smp_per(c,'r6', c.upper,self.chunk.getAttribute('semprop'),self.chunk.person)
                                    if True:
                                        if ans!=None and self.chunk.isPronoun and self.chunk!=ans:
                                            self.chunk.PrePronounrefstm=ans.upper
                                            self.chunk.PrePronounrefnode=ans
                                            self.chunk.isPronounresolved=True
                                            break
                    
                    for c in reversed(a.nodeList):
                        if (c.name==self.chunk.name and self.chunk.isPronoun and  not self.chunk.isPronounresolved) or flagchunk:
                            flagchunk=True
                            if c.name==self.chunk.name:
                                continue
                            for d in reversed(c.nodeList):
                                if d.type=='VM':
                                    ans=self.find_dep_in_stm_W_smp_per(c,'k2', c.upper,self.chunk.getAttribute('semprop'),self.chunk.person)
                                    if True:
                                        if ans!=None and self.chunk.isPronoun and self.chunk!=ans:
                                            self.chunk.PrePronounrefstm=ans.upper
                                            self.chunk.PrePronounrefnode=ans
                                            self.chunk.isPronounresolved=True
                                            break
                    for c in reversed(a.nodeList):
                        if (c.name==self.chunk.name and self.chunk.isPronoun and  not self.chunk.isPronounresolved) or flagchunk:
                            flagchunk=True
                            if c.name==self.chunk.name:
                                continue
                            for d in reversed(c.nodeList):
                                if d.type=='VM':
                                    ans=self.find_dep_in_stm_W_smp_per(c,'k4', c.upper,self.chunk.getAttribute('semprop'),self.chunk.person)
                                    if True:
                                        if ans!=None and self.chunk.isPronoun and self.chunk!=ans:
                                            self.chunk.PrePronounrefstm=ans.upper
                                            self.chunk.PrePronounrefnode=ans
                                            self.chunk.isPronounresolved=True
                                            break
                                        
                    for c in reversed(a.nodeList):
                        if (c.name==self.chunk.name and self.chunk.isPronoun and  not self.chunk.isPronounresolved) or flagchunk:
                            flagchunk=True
                            if c.name==self.chunk.name:
                                continue
                            for d in reversed(c.nodeList):
                                if d.type=='VM':
                                    ans=self.find_dep_in_stm_W_smp_per(c,'ccof', c.upper,self.chunk.getAttribute('semprop'),self.chunk.person)
                                    if True:
                                        if ans!=None and self.chunk.isPronoun and self.chunk!=ans:
                                            self.chunk.PrePronounrefstm=ans.upper
                                            self.chunk.PrePronounrefnode=ans
                                            self.chunk.isPronounresolved=True
                                            break
                
                
                for c in reversed(a.nodeList):
                        if (c.name==self.chunk.name and self.chunk.isPronoun and  not self.chunk.isPronounresolved) or flagchunk:
                            flagchunk=True
                            if c.name==self.chunk.name:
                                continue
                            if c.type=='VGNN'and c.parentRelation=='r6':
                                    ans=c
                                    if True:
                                        if ans!=None and self.chunk.isPronoun and self.chunk!=ans:
                                            self.chunk.PrePronounrefstm=ans.upper
                                            self.chunk.PrePronounrefnode=ans
                                            self.chunk.isPronounresolved=True
                                            break
                    
        return self.chunk                        
                                
                                
    def find_dep(self,depL,stmL):
        ans=None
        for a in stmL.nodeList:
            if a.parentRelation==depL:
                ans=a
                break
        return ans
    def find_dep_in_stm(self,node,depL,stmL):
        ans=None
        for a in stmL.nodeList:
            if node.name==a.parent and a.parentRelation==depL:
                ans=a
                break
        return ans
    def find_dep_in_stm_W_smp(self,node,depL,stmL,semprop):
        ans=None
        for a in stmL.nodeList:
            if node.name==a.parent and a.parentRelation==depL and a.getAttribute('semprop')==semprop:
                ans=a
                break
        return ans
    
    def find_dep_in_stm_W_smp_emax_num(self,node,depL,stmL,semprop,emax,number):
        ans=None
        for a in stmL.nodeList:
            if node.name==a.parent and a.parentRelation==depL and a.getAttribute('semprop')==semprop and a.getAttribute('enamex_type')==emax and a.number==number:
                ans=a
                break
        return ans
    def find_dep_in_stm_W_smp_per(self,node,depL,stmL,semprop,person):
        ans=None
        for a in stmL.nodeList:
            if node.name==a.parent and a.parentRelation==depL and a.getAttribute('semprop')==semprop and a.person==person:
                ans=a
                break
        return ans
    def find_dep_in_stm_W_smp_per1(self,node,depL,stmL,semprop,person):
        ans=None
        for a in stmL.nodeList:
            if node.name==a.parent and a.parentRelation==depL and a.getAttribute('semprop')==semprop and (a.person==person or (person=='3h' and a.person=='3')or (person=='3' and a.person=='3h')):
                ans=a
                break
        return ans
    def find_PRP_in_stm(self,PRP_root,chunk):
        ans=None
        flag=False
        for a in reversed(chunk.upper.nodeList):
            if flag:
                for b in a.nodeList:
                    if b.morphroot in PRP_root:
                        ans=a
            if a.name==chunk.name:
                flag=True
        return ans

class thirdPerson_resolution():
    
    def __init__(self, node,chunk):
        self.node=node
        self.chunk=chunk
        self.tree=chunk.upper
        self.file=self.tree.upper
        #self.TPResolve()
        
    def TPResolve(self):
        #print 'in third person resolution'
        flagsentence=False
        flagchunk=False
        # looking into same sentence 
        for a in reversed(self.file.nodeList):
            if (a.name==self.tree.name or flagsentence) and not self.chunk.isPronounresolved:
                flagsentence=True
                if int(a.name)-int(self.tree.name)==0:
                    #start of JO0
                    # end of JOO
                    for c in reversed(a.nodeList):
                        if (c.name==self.chunk.name and self.chunk.isPronoun and  not self.chunk.isPronounresolved) or flagchunk:
                            flagchunk=True
                            if c.name==self.chunk.name:
                                continue
                            for d in reversed(c.nodeList):
                                if d.type=='VM':
                                    ans=self.find_dep_in_stm_W_smp(c,'k1u', c.upper,self.chunk.getAttribute('semprop'))
                                    if ans!=None and self.chunk.isPronoun and self.chunk!=ans:
                                        if ans.isPronoun:
                                            self.chunk.PrePronounrefstm=ans.upper
                                            self.chunk.PrePronounrefnode=ans
                                            self.chunk.isPronounresolved=True
                                            break
                    # Start of JoO

                    # end of JO
                    flagchunk=False
                    for c in reversed(a.nodeList):
                            if (c.name==self.chunk.name and self.chunk.isPronoun and  not self.chunk.isPronounresolved) or flagchunk:
                                flagchunk=True
                                if c.name==self.chunk.name:
                                    continue
                                if (c.parentRelation=='pof') and re.search(r'VGF.',c.parent,re.M|re.I)and self.chunk.getAttribute('semprop')==c.getAttribute('semprop') and c.morphPOS=='n':
                                        ans=c
                                        if ans!=None and self.chunk.isPronoun and self.chunk!=ans:
                                            self.chunk.PrePronounrefstm=ans.upper
                                            self.chunk.PrePronounrefnode=ans
                                            self.chunk.isPronounresolved=True
                                            break
                    flagchunk=False
                    for c in reversed(a.nodeList):
                            if (c.name==self.chunk.name and self.chunk.isPronoun and  not self.chunk.isPronounresolved) or flagchunk:
                                flagchunk=True
                                if c.name==self.chunk.name:
                                    continue
                                if (c.parentRelation=='vmod') and re.search(r'VGF',c.parent,re.M|re.I)and self.chunk.getAttribute('semprop')==c.getAttribute('semprop') and c.morphPOS=='n':
                                        ans=c
                                        if ans!=None and self.chunk.isPronoun and self.chunk!=ans:
                                            self.chunk.PrePronounrefstm=ans.upper
                                            self.chunk.PrePronounrefnode=ans
                                            self.chunk.isPronounresolved=True
                                            break
                    PRP_HValue=None
                    
                    for e in (self.chunk.nodeList):
                        if e.type=='PRP':
                            PRP_HValue=e.lex
                    
                    chunk_no_count=0
                    flagchunk=False
                    for c in reversed(a.nodeList):
                            if (c.name==self.chunk.name and self.chunk.isPronoun and  not self.chunk.isPronounresolved) or flagchunk:
                                flagchunk=True
                                if c.name==self.chunk.name:
                                    chunk_no_count=0
                                    continue
                                else:
                                    chunk_no_count=chunk_no_count+1
				

                                    for d in c.nodeList:
                                        if d.lex=='':
                                            continue
                                    if chunk_no_count>7:
                                        break
                                    if PRP_HValue =='' and (c.parentRelation=='r6') and self.chunk.number==c.number and self.chunk.getAttribute('semprop')==c.getAttribute('semprop') and c.morphPOS=='n':
                                        ans=c
                                        if ans!=None and self.chunk.isPronoun and self.chunk!=ans:
                                            self.chunk.PrePronounrefstm=ans.upper
                                            self.chunk.PrePronounrefnode=ans
                                            self.chunk.isPronounresolved=True
                                            break
                    flagchunk=False
                    for c in reversed(a.nodeList):
                        if (c.name==self.chunk.name and self.chunk.isPronoun and  not self.chunk.isPronounresolved) or flagchunk:
                            flagchunk=True
                            if c.name==self.chunk.name:
                                continue
                            for d in reversed(c.nodeList):
                                if d.type=='VM':
                                    ans=self.find_dep_in_stm_W_smp_emax_num(c,'k1', c.upper,self.chunk.getAttribute('semprop'),'person',self.chunk.number)
                                    if True:
                                        if ans!=None and self.chunk.isPronoun and self.chunk!=ans:
                                            self.chunk.PrePronounrefstm=ans.upper
                                            self.chunk.PrePronounrefnode=ans
                                            self.chunk.isPronounresolved=True
                                            break
                    flagchunk=False
                    for c in reversed(a.nodeList):
                        if (c.name==self.chunk.name and self.chunk.isPronoun and  not self.chunk.isPronounresolved) or flagchunk:
                            flagchunk=True
                            if c.name==self.chunk.name:
                                continue
                            for d in reversed(c.nodeList):
                                if d.type=='VM':
                                    ans=self.find_dep_in_stm_W_smp(c,'k1', c.upper,self.chunk.getAttribute('semprop'))
                                    if True:
                                        if ans!=None and self.chunk.isPronoun and self.chunk!=ans:
                                            self.chunk.PrePronounrefstm=ans.upper
                                            self.chunk.PrePronounrefnode=ans
                                            self.chunk.isPronounresolved=True
                                            break
                    flagchunk=False
                    for c in reversed(a.nodeList):
                        if (c.name==self.chunk.name and self.chunk.isPronoun and  not self.chunk.isPronounresolved) or flagchunk:
                            flagchunk=True
                            if c.name==self.chunk.name:
                                continue
                            for d in reversed(c.nodeList):
                                if d.type=='VM':
                                    ans=self.find_dep_in_stm_W_smp(c,'r6', c.upper,self.chunk.getAttribute('semprop'))
                                    if True:
                                        if ans!=None and self.chunk.isPronoun and self.chunk!=ans:
                                            self.chunk.PrePronounrefstm=ans.upper
                                            self.chunk.PrePronounrefnode=ans
                                            self.chunk.isPronounresolved=True
                                            break
                    flagchunk=False
                    for c in reversed(a.nodeList):
                        if (c.name==self.chunk.name and self.chunk.isPronoun and  not self.chunk.isPronounresolved) or flagchunk:
                            flagchunk=True
                            if c.name==self.chunk.name:
                                continue
                            for d in reversed(c.nodeList):
                                if d.type=='VM':
                                    ans=self.find_dep_in_stm_W_smp(c,'k2', c.upper,self.chunk.getAttribute('semprop'))
                                    if True:
                                        if ans!=None and self.chunk.isPronoun and self.chunk!=ans:
                                            self.chunk.PrePronounrefstm=ans.upper
                                            self.chunk.PrePronounrefnode=ans
                                            self.chunk.isPronounresolved=True
                                            break
                    flagchunk=False
                    for c in reversed(a.nodeList):
                        if (c.name==self.chunk.name and self.chunk.isPronoun and  not self.chunk.isPronounresolved) or flagchunk:
                            flagchunk=True
                            if c.name==self.chunk.name:
                                continue
                                if (c.parentRelation=='rt') and self.chunk.getAttribute('semprop')==c.getAttribute('semprop'):    
                                    if True:
                                        if ans!=None and self.chunk.isPronoun and self.chunk!=ans:
                                            self.chunk.PrePronounrefstm=ans.upper
                                            self.chunk.PrePronounrefnode=ans
                                            self.chunk.isPronounresolved=True
                                            break
                    flagchunk=False
                    for c in reversed(a.nodeList):
                        if (c.name==self.chunk.name and self.chunk.isPronoun and  not self.chunk.isPronounresolved) or flagchunk:
                            flagchunk=True
                            if c.name==self.chunk.name:
                                continue
                                if (c.parentRelation=='ccof') and self.chunk.getAttribute('semprop')==c.getAttribute('semprop'):    
                                    if True:
                                        if ans!=None and self.chunk.isPronoun and self.chunk!=ans:
                                            self.chunk.PrePronounrefstm=ans.upper
                                            self.chunk.PrePronounrefnode=ans
                                            self.chunk.isPronounresolved=True
                                            break
                    flagchunk=False
                    for c in reversed(a.nodeList):
                        if (c.name==self.chunk.name and self.chunk.isPronoun and  not self.chunk.isPronounresolved) or flagchunk:
                            flagchunk=True
                            if c.name==self.chunk.name:
                                continue
                                if (c.parentRelation=='r6-k2') and self.chunk.getAttribute('semprop')==c.getAttribute('semprop'):    
                                    if True:
                                        if ans!=None and self.chunk.isPronoun and self.chunk!=ans:
                                            self.chunk.PrePronounrefstm=ans.upper
                                            self.chunk.PrePronounrefnode=ans
                                            self.chunk.isPronounresolved=True
                                            break
                    flagchunk=False
                    for c in reversed(a.nodeList):
                        if (c.name==self.chunk.name and self.chunk.isPronoun and  not self.chunk.isPronounresolved) or flagchunk:
                            flagchunk=True
                            if c.name==self.chunk.name:
                                continue
                                if (c.parentRelation=='r6') and self.chunk.getAttribute('semprop')==c.getAttribute('semprop'):    
                                    if True:
                                        if ans!=None and self.chunk.isPronoun and self.chunk!=ans:
                                            self.chunk.PrePronounrefstm=ans.upper
                                            self.chunk.PrePronounrefnode=ans
                                            self.chunk.isPronounresolved=True
                                            break
                    
                    flagchunk=False
                    for c in reversed(a.nodeList):
                        if (c.name==self.chunk.name and self.chunk.isPronoun and  not self.chunk.isPronounresolved) or flagchunk:
                            flagchunk=True
                            if c.name==self.chunk.name:
                                continue
                                if (c.parentRelation=='ccof') and self.chunk.getAttribute('semprop')==c.getAttribute('semprop'):    
                                    if True:
                                        if ans!=None and self.chunk.isPronoun and self.chunk!=ans:
                                            self.chunk.PrePronounrefstm=ans.upper
                                            self.chunk.PrePronounrefnode=ans
                                            self.chunk.isPronounresolved=True
                                            break
                #end looking into same sentence
                #start looking into -1 sentence
                if int(self.tree.name)-int(a.name)==1:
                    for c in reversed(a.nodeList):
                        if (c.name==self.chunk.name and self.chunk.isPronoun and  not self.chunk.isPronounresolved) or flagchunk:
                            flagchunk=True
                            if c.name==self.chunk.name:
                                continue
                                if (c.parentRelation=='k2') and self.chunk.getAttribute('semprop')==c.getAttribute('semprop') and self.chunk.number==c.number and self.chunk.person==c.person:    
                                    if True:
                                        if ans!=None and self.chunk.isPronoun and self.chunk!=ans:
                                            self.chunk.PrePronounrefstm=ans.upper
                                            self.chunk.PrePronounrefnode=ans
                                            self.chunk.isPronounresolved=True
                                            break
                    
                    for c in reversed(a.nodeList):
                        if (c.name==self.chunk.name and self.chunk.isPronoun and  not self.chunk.isPronounresolved) or flagchunk:
                            flagchunk=True
                            if c.name==self.chunk.name:
                                continue
                            for d in reversed(c.nodeList):
                                if d.type=='VM':
                                    ans=self.find_dep_in_stm_W_smp_per(c,'k1', c.upper,self.chunk.getAttribute('semprop'),self.chunk.person)
                                    if True:
                                        if ans!=None and self.chunk.isPronoun and self.chunk!=ans:
                                            self.chunk.PrePronounrefstm=ans.upper
                                            self.chunk.PrePronounrefnode=ans
                                            self.chunk.isPronounresolved=True
                                            break
                    
                    for c in reversed(a.nodeList):
                        if (c.name==self.chunk.name and self.chunk.isPronoun and  not self.chunk.isPronounresolved) or flagchunk:
                            flagchunk=True
                            if c.name==self.chunk.name:
                                continue
                            for d in reversed(c.nodeList):
                                if d.type=='VM':
                                    ans=self.find_dep_in_stm_W_smp_per(c,'r6', c.upper,self.chunk.getAttribute('semprop'),self.chunk.person)
                                    if True:
                                        if ans!=None and self.chunk.isPronoun and self.chunk!=ans:
                                            self.chunk.PrePronounrefstm=ans.upper
                                            self.chunk.PrePronounrefnode=ans
                                            self.chunk.isPronounresolved=True
                                            break
                    
                    for c in reversed(a.nodeList):
                        if (c.name==self.chunk.name and self.chunk.isPronoun and  not self.chunk.isPronounresolved) or flagchunk:
                            flagchunk=True
                            if c.name==self.chunk.name:
                                continue
                            for d in reversed(c.nodeList):
                                if d.type=='VM':
                                    ans=self.find_dep_in_stm_W_smp_per(c,'k2', c.upper,self.chunk.getAttribute('semprop'),self.chunk.person)
                                    if True:
                                        if ans!=None and self.chunk.isPronoun and self.chunk!=ans:
                                            self.chunk.PrePronounrefstm=ans.upper
                                            self.chunk.PrePronounrefnode=ans
                                            self.chunk.isPronounresolved=True
                                            break
                    
                    for c in reversed(a.nodeList):
                        if (c.name==self.chunk.name and self.chunk.isPronoun and  not self.chunk.isPronounresolved) or flagchunk:
                            flagchunk=True
                            if c.name==self.chunk.name:
                                continue
                            for d in reversed(c.nodeList):
                                if d.type=='VM':
                                    ans=self.find_dep_in_stm_W_smp_per(c,'k4', c.upper,self.chunk.getAttribute('semprop'),self.chunk.person)
                                    if True:
                                        if ans!=None and self.chunk.isPronoun and self.chunk!=ans:
                                            self.chunk.PrePronounrefstm=ans.upper
                                            self.chunk.PrePronounrefnode=ans
                                            self.chunk.isPronounresolved=True
                                            break
                    
                    for c in reversed(a.nodeList):
                        if (c.name==self.chunk.name and self.chunk.isPronoun and  not self.chunk.isPronounresolved) or flagchunk:
                            flagchunk=True
                            if c.name==self.chunk.name:
                                continue
                            for d in reversed(c.nodeList):
                                if d.type=='VM':
                                    ans=self.find_dep_in_stm_W_smp_per(c,'ccof', c.upper,self.chunk.getAttribute('semprop'),self.chunk.person)
                                    if True:
                                        if ans!=None and self.chunk.isPronoun and self.chunk!=ans:
                                            self.chunk.PrePronounrefstm=ans.upper
                                            self.chunk.PrePronounrefnode=ans
                                            self.chunk.isPronounresolved=True
                                            break
                    
                    for c in reversed(a.nodeList):
                        if (c.name==self.chunk.name and self.chunk.isPronoun and  not self.chunk.isPronounresolved) or flagchunk:
                            flagchunk=True
                            if c.name==self.chunk.name:
                                continue
                            for d in reversed(c.nodeList):
                                if d.type=='VM':
                                    if c.type=='VGF':
                                        ans=self.find_dep_in_stm_W_smp_per(c,'k1', c.upper,self.chunk.getAttribute('semprop'),self.chunk.person)
                                        if ans!=None and self.chunk.isPronoun and self.chunk!=ans:
                                            self.chunk.PrePronounrefstm=ans.upper
                                            self.chunk.PrePronounrefnode=ans
                                            self.chunk.isPronounresolved=True
                                            break
                                        
                # Start of JoO
                # end of JO                
                #end looking into -1 sentence
                #start looking into -2 sentence
                if int(self.tree.name)-int(a.name)==2:
                    for c in reversed(a.nodeList):
                        if (c.name==self.chunk.name and self.chunk.isPronoun and  not self.chunk.isPronounresolved) or flagchunk:
                            flagchunk=True
                            if c.name==self.chunk.name:
                                continue
                            for d in reversed(c.nodeList):
                                if d.type=='VM':
                                    ans=self.find_dep_in_stm_W_smp_per(c,'k1', c.upper,self.chunk.getAttribute('semprop'),self.chunk.person)
                                    if True:
                                        if ans!=None and self.chunk.isPronoun and self.chunk!=ans:
                                            self.chunk.PrePronounrefstm=ans.upper
                                            self.chunk.PrePronounrefnode=ans
                                            self.chunk.isPronounresolved=True
                                            break
                    for c in reversed(a.nodeList):
                        if (c.name==self.chunk.name and self.chunk.isPronoun and  not self.chunk.isPronounresolved) or flagchunk:
                            flagchunk=True
                            if c.name==self.chunk.name:
                                continue
                            for d in reversed(c.nodeList):
                                if d.type=='VM':
                                    ans=self.find_dep_in_stm_W_smp_per(c,'r6', c.upper,self.chunk.getAttribute('semprop'),self.chunk.person)
                                    if True:
                                        if ans!=None and self.chunk.isPronoun and self.chunk!=ans:
                                            self.chunk.PrePronounrefstm=ans.upper
                                            self.chunk.PrePronounrefnode=ans
                                            self.chunk.isPronounresolved=True
                                            break
                    
                    for c in reversed(a.nodeList):
                        if (c.name==self.chunk.name and self.chunk.isPronoun and  not self.chunk.isPronounresolved) or flagchunk:
                            flagchunk=True
                            if c.name==self.chunk.name:
                                continue
                            for d in reversed(c.nodeList):
                                if d.type=='VM':
                                    ans=self.find_dep_in_stm_W_smp_per(c,'k2', c.upper,self.chunk.getAttribute('semprop'),self.chunk.person)
                                    if True:
                                        if ans!=None and self.chunk.isPronoun and self.chunk!=ans:
                                            self.chunk.PrePronounrefstm=ans.upper
                                            self.chunk.PrePronounrefnode=ans
                                            self.chunk.isPronounresolved=True
                                            break
                    for c in reversed(a.nodeList):
                        if (c.name==self.chunk.name and self.chunk.isPronoun and  not self.chunk.isPronounresolved) or flagchunk:
                            flagchunk=True
                            if c.name==self.chunk.name:
                                continue
                            for d in reversed(c.nodeList):
                                if d.type=='VM':
                                    ans=self.find_dep_in_stm_W_smp_per(c,'k4', c.upper,self.chunk.getAttribute('semprop'),self.chunk.person)
                                    if True:
                                        if ans!=None and self.chunk.isPronoun and self.chunk!=ans:
                                            self.chunk.PrePronounrefstm=ans.upper
                                            self.chunk.PrePronounrefnode=ans
                                            self.chunk.isPronounresolved=True
                                            break
                                        
                    for c in reversed(a.nodeList):
                        if (c.name==self.chunk.name and self.chunk.isPronoun and  not self.chunk.isPronounresolved) or flagchunk:
                            flagchunk=True
                            if c.name==self.chunk.name:
                                continue
                            for d in reversed(c.nodeList):
                                if d.type=='VM':
                                    ans=self.find_dep_in_stm_W_smp_per(c,'ccof', c.upper,self.chunk.getAttribute('semprop'),self.chunk.person)
                                    if True:
                                        if ans!=None and self.chunk.isPronoun and self.chunk!=ans:
                                            self.chunk.PrePronounrefstm=ans.upper
                                            self.chunk.PrePronounrefnode=ans
                                            self.chunk.isPronounresolved=True
                                            break
                
                
                for c in reversed(a.nodeList):
                        if (c.name==self.chunk.name and self.chunk.isPronoun and  not self.chunk.isPronounresolved) or flagchunk:
                            flagchunk=True
                            if c.name==self.chunk.name:
                                continue
                            if c.type=='VGNN'and c.parentRelation=='r6':
                                    ans=c
                                    if True:
                                        if ans!=None and self.chunk.isPronoun and self.chunk!=ans:
                                            self.chunk.PrePronounrefstm=ans.upper
                                            self.chunk.PrePronounrefnode=ans
                                            self.chunk.isPronounresolved=True
                                            break
                    
        return self.chunk                        
                                
                                
    def find_dep(self,depL,stmL):
        ans=None
        for a in stmL.nodeList:
            if a.parentRelation==depL:
                ans=a
                break
        return ans
    def find_dep_in_stm(self,node,depL,stmL):
        ans=None
        for a in stmL.nodeList:
            if node.name==a.parent and a.parentRelation==depL:
                ans=a
                break
        return ans
    def find_dep_in_stm_W_smp(self,node,depL,stmL,semprop):
        ans=None
        for a in stmL.nodeList:
            if node.name==a.parent and a.parentRelation==depL and a.getAttribute('semprop')==semprop:
                ans=a
                break
        return ans
    
    def find_dep_in_stm_W_smp_emax_num(self,node,depL,stmL,semprop,emax,number):
        ans=None
        for a in stmL.nodeList:
            if node.name==a.parent and a.parentRelation==depL and a.getAttribute('semprop')==semprop and a.getAttribute('enamex_type')==emax and a.number==number:
                ans=a
                break
        return ans
    def find_dep_in_stm_W_smp_per(self,node,depL,stmL,semprop,person):
        ans=None
        for a in stmL.nodeList:
            if node.name==a.parent and a.parentRelation==depL and a.getAttribute('semprop')==semprop and a.person==person:
                ans=a
                break
        return ans
    def find_dep_in_stm_W_smp_per1(self,node,depL,stmL,semprop,person):
        ans=None
        for a in stmL.nodeList:
            if node.name==a.parent and a.parentRelation==depL and a.getAttribute('semprop')==semprop and (a.person==person or (person=='3h' and a.person=='3')or (person=='3' and a.person=='3h')):
                ans=a
                break
        return ans
    def find_PRP_in_stm(self,PRP_root,chunk):
        ans=None
        flag=False
        for a in reversed(chunk.upper.nodeList):
            if flag:
                for b in a.nodeList:
                    if b.morphroot in PRP_root:
                        ans=a
            if a.name==chunk.name:
                flag=True
        return ans

class reflexive_resolution():
    
    def __init__(self, node):
        self.node=node
        self.chunk=self.node.upper
        self.tree=self.chunk.upper
        self.file=self.tree.upper
        self.reflexiveList=[u'अपने',u'अपना',u'अपनी',u'खुद']
        self.reflexivePossessiveList=[u'अपने',u'अपना',u'अपनी']
        self.placeList=[u'वहां',u'यहां',u'जहां',u'वहाँ']
        #self.RFResolve()
        
    def RFResolve(self):
        if self.node.lex in self.reflexiveList:
            Ref= self.searchSibling()
            if Ref==None:
                REf1= self.searchUpwards(self.chunk)
                if REf1==None:
                    REf2=self.searchUpwards(self.chunk)
        return self.chunk
                
    def searchUpwards(self,currentnode):
        if currentnode!=None:
            if currentnode.parent!='0':
                if not self.isRefkexivepossesive(self.node.morphroot) and currentnode.parent!='0':
                    if self.checkReflexiveReferents(self.chunknametochunkrefmapping(currentnode.parent)):
                        #print 'return 1',self.chunknametochunkrefmapping(currentnode.parent).name
                        if not self.chunk.isPronounresolved:
                                self.chunk.PrePronounrefstm=self.chunknametochunkrefmapping(currentnode.parent).upper
                                self.chunk.PrePronounrefnode=self.chunknametochunkrefmapping(currentnode.parent)
                                self.chunk.isPronounresolved=True
                        return self.chunknametochunkrefmapping(currentnode.parent)
                    
                for c in currentnode.upper.nodeList:
                    if c.parent==currentnode.parent and currentnode.name!=c.name:
                        #print c.name , c.getAttribute('head') , 'in search upword'
                        if self.checkReflexiveReferents(c):
                            if c.type=='CCP':
                                for c1 in c.upper.nodeList:
                                    if c1.parent==c.parent and c.name!=c1.name:
                                        if c1.parentRelation=='ccof':
                                            #print 'return 2',c1.name
                                            if not self.chunk.isPronounresolved:
                                                self.chunk.PrePronounrefstm=c1.upper
                                                self.chunk.PrePronounrefnode=c1
                                                self.chunk.isPronounresolved=True
                                            return c1
                            #print 'return 3',c.name
                            if not self.chunk.isPronounresolved:
                                self.chunk.PrePronounrefstm=c.upper
                                self.chunk.PrePronounrefnode=c
                                self.chunk.isPronounresolved=True
                            return c
                
                
                self.searchUpwards(self.chunknametochunkrefmapping(currentnode.parent))    
            
        '''
        if currentnode.parent=='0':
            return None
        else:
            self.searchUpwards(self.chunknametochunkrefmapping(currentnode.parent))
        '''
    def checkReflexiveReferents(self,currentnode):
        ref=False
        if currentnode.parentRelation=='k1' or currentnode.parentRelation=='k4'or (currentnode.parentRelation=='k2' and currentnode.getAttribute('semprop')=='h'):
            ref= True
        return ref
        
    def chunknametochunkrefmapping(self,parent):
        #print 'in chunknametochunkrefmapping'
        for c in self.chunk.upper.nodeList:
            if c.name==parent:
                return c
    
    def searchSibling(self):
        for c in self.chunk.upper.nodeList:
            if c.parent==self.chunk.parent and self.chunk.name!=c.name:
                if c.parentRelation=='r6':
                    self.chunk.PrePronounrefstm=c.upper
                    self.chunk.PrePronounrefnode=c
                    self.chunk.isPronounresolved=True
                    #print 'return 0',c.name
                    return c
        return None
        
            
        
    def isresolvable(self):
        print 'in is resolvable'
        
    def isreflexive(self):
        print 'in is reflexive'
        print 'check in reflexiveList'

    def isRefkexivepossesive(self,word):
        if word in self.reflexivePossessiveList:
            return True
        else:
            return False
        
        

class ML_base_resolution():
    def __init__(self, node,PRPTYPE,modelpath):
        self.node=node
        self.chunk=self.node.upper
        self.tree=self.chunk.upper
        self.file=self.tree.upper
        self.PRPTYPE=PRPTYPE
	self.modelpath=modelpath
        #print '\n\n\n\n\n\n'
        #self.MLResolve()
        
    def MLResolve(self):
        flagsentence=False
        flagchunk=False
        flagchunk1=True
        chunkdistance=0
        stmdistance=0
        count=0
        testDATASET={}
        countTochunkmapping={}
        # looking into same sentence 
        for a in reversed(self.file.nodeList):
            if (a.name==self.tree.name or flagsentence) and not self.chunk.isPronounresolved:
                flagsentence=True
                if int(self.tree.name)-int(a.name)<4:
                    #print 'sentence changed', a.name , self.tree.name
                    chunk_head=''
                    for c in reversed(a.nodeList):
                        
                        if c.type=='NP':
                            if (c.name==self.chunk.name and self.chunk.isPronoun and  not self.chunk.isPronounresolved) or flagchunk:
                                flagchunk=True
                                if c.name==self.chunk.name and flagchunk1:
                                    chunkdistance=0
                                    flagchunk1=False
                                    continue
                                
                                chunkdistance=chunkdistance+1
                                #print c.name, c.upper.name,c.getAttribute('head')
                                for d in c.nodeList:
                                    if c.type=='DEM':
                                        DEM=1
                                    else:
                                        DEM=0
                                    if c.morphroot==d.morphroot and d.lex!=None:
                                        chunk_head=d.type
                                        if 'PRP' not in chunk_head  or 'NNP' not in chunk_head or 'NNPC' not in chunk_head or 'NN' not in chunk_head or 'NNC' not in chunk_head:
                                            continue
                                        if chunk_head=='':
                                            continue
                                
                                
                                 
                                if chunk_head=='NN' or chunk_head=='NNC':
                                    FNN=1
                                else:
                                    FNN=0
                                
                                if chunk_head=='NNP' or chunk_head=='NNPC':
                                    FNNP=1
                                else:
                                    FNNP=0
                                
                                if chunk_head=='PRP':
                                    FPRP=1
                                else:
                                    FPRP=0
                                
                                if FPRP==0 and FNN==0 and FNNP==0:
                                    FOTR=1
                                else:
                                    FOTR=0
                                
                                stmdistance=int(self.tree.name)-int(a.name)
                                
                                
                                
                                PRPchunksemprop=self.chunk.getAttribute('semprop')
                                if PRPchunksemprop=='' or PRPchunksemprop==None:
                                    PRPchunksemprop='rest'
                                PRPchunknumber=self.chunk.number
                                if PRPchunknumber=='' or PRPchunknumber==None:
                                    PRPchunknumber='rest'
                                PRPchunkperson=self.chunk.person
                                if PRPchunkperson=='' or PRPchunkperson==None:
                                    PRPchunkperson='rest'
                                
                                
                                
                                chunksemprop=c.getAttribute('semprop')
                                if chunksemprop=='' or chunksemprop==None:
                                    chunksemprop='rest'
                                chunknumber=c.number
                                if chunknumber=='' or chunknumber==None:
                                    chunknumber='any'
                                chunkperson=c.person
                                if chunkperson=='' or chunkperson==None:
                                    chunkperson='none'
                                

                                testDATASET[count]={}
                                testDATASET[count]['anaphora']=self.node.lex
                                testDATASET[count]['anaPRP_type']=self.PRPTYPE
                                testDATASET[count]['anasemprop']=PRPchunksemprop
                                testDATASET[count]['anaperson']=PRPchunkperson
                                testDATASET[count]['ananumber']=PRPchunknumber
                                testDATASET[count]['antPOSTag']=chunk_head
                                testDATASET[count]['antNN']=int(FNN)
                                testDATASET[count]['antNNP']=int(FNNP)
                                testDATASET[count]['antPRP']=int(FPRP)
                                testDATASET[count]['antOTR']=int(FOTR)
                                testDATASET[count]['antsemprop']=chunksemprop
                                testDATASET[count]['antperson']=chunkperson
                                testDATASET[count]['antnumber']=chunknumber
                                testDATASET[count]['distance']=int(chunkdistance)
                                testDATASET[count]['stm_distance']=int(stmdistance)
                                testDATASET[count]['DEM']=int(DEM)
                                countTochunkmapping[count]=c
                                count=count+1
        
        dis=[]
        ans=None
        
        with open(self.modelpath, 'rb') as fid:
            gnb_loaded = cPickle.load(fid)
        for i in testDATASET:
            #print testDATASET[i]
            dis=[]
            dis.append(testDATASET[i])
            #print testDATASET[i]

            vec=DictVectorizer(sparse=False)
	    vec.vocabulary_={u'anaphora=\u091c\u093f\u0938\u0928\u0947': 61, 'anaPRP_type=L': 3, 'antNNP': 95, 'anasemprop=rest': 93, u'anaphora=\u0939\u092e\u0928\u0947': 83, 'antperson=3h': 108, u'anaphora=\u0924\u092e': 66, u'anaphora=\u0906\u092a\u0938\u0947': 27, 'ananumber=pl': 9, 'anaPRP_type=JO': 2, 'anasemprop=': 89, 'stm_distance': 117, 'anasemprop=in': 92, 'anaperson=1': 11, u'anaphora=\u091c\u093f\u0938\u0938\u0947': 63, 'anaperson=3': 14, 'anaperson=2': 12, u'anaphora=\u091c\u093f\u0928\u094d\u0939\u0947\u0902': 55, 'antOTR': 96, u'anaphora=\u0909\u0938\u092e\u0947\u0902': 42, u'anaphora=\u091c\u093f\u0938': 57, u'anaphora=\u091c\u094b': 65, 'ananumber=sg': 10, u'anaphora=\u0906\u092a\u0938': 26, 'antPOSTag=NN': 97, u'anaphora=\u0935\u0939\u0940': 78, u'anaphora=\u091c\u093f\u0928': 50, 'antsemprop=h': 113, u'anaphora=\u0915\u093f\u0938\u0915\u0947': 46, u'anaphora=\u0906\u092a\u0915\u0940': 23, u'anaphora=\u0906\u092a\u0915\u0947': 24, u'anaphora=\u0906\u092a\u0915\u094b': 25, 'antnumber=sg': 103, 'DEM': 0, u'anaphora=\u091c\u093f\u0928\u092e\u0947\u0902': 53, u'anaphora=\u0935\u0939\u093e\u0902': 77, u'anaphora=\u0935\u0939\u093e\u0901': 76, 'antsemprop=nh': 114, 'anaPRP_type=SP': 6, u'anaphora=\u0909\u0928\u0938\u0947': 34, u'anaphora=\u0939\u092e': 82, u'anaphora=\u0916\u0941\u0926': 47, u'anaphora=\u0939\u092e\u0947\u0902': 88, u'anaphora=\u092e\u0947\u0930\u093e': 70, u'anaphora=\u091c\u093f\u0928\u094d\u0939\u094b\u0902\u0928\u0947': 56, 'ananumber=any': 8, 'antsemprop=': 111, u'anaphora=\u092e\u0941\u091d\u0938\u0947': 68, u'anaphora=\u0909\u0938\u0940': 44, 'antperson=1': 104, 'antperson=2': 105, 'antperson=3': 107, u'anaphora=\u0906\u092a': 21, u'anaphora=\u092e\u0947\u0930\u0940': 71, u'anaphora=\u092e\u0947\u0930\u0947': 72, u'anaphora=\u091c\u0939\u093e\u0901': 48, u'anaphora=\u091c\u0939\u093e\u0902': 49, u'anaphora=\u0939\u092e\u0938\u0947': 84, 'anaperson=any': 16, u'anaphora=\u0935\u0939\u0940\u0902': 79, 'antperson=2h': 106, 'antPOSTag=PRP': 99, u'anaphora=\u0906\u092a\u0915\u093e': 22, u'anaphora=\u091c\u093f\u0938\u0915\u093e': 58, 'antPRP': 100, u'anaphora=\u0909\u0928\u094d\u0939\u094b\u0902\u0928\u0947': 36, u'anaphora=\u091c\u093f\u0938\u0947': 64, u'anaphora=\u092e\u0948\u0902\u0928\u0947': 74, u'anaphora=\u092e\u0941\u091d\u0947': 69, 'anaPRP_type=FP': 1, u'anaphora=\u091c\u093f\u0938\u092e\u0947\u0902': 62, u'anaphora=\u0909\u0938\u0947': 45, u'anaphora=\u091c\u093f\u0928\u0915\u0947': 52, u'anaphora=\u091c\u093f\u0928\u0915\u0940': 51, 'antNN': 94, u'anaphora=\u0909\u0938': 37, 'antnumber=any': 101, u'anaphora=\u0909\u0928': 28, 'antsemprop=any': 112, u'anaphora=\u0909\u0928\u094d\u0939\u0947\u0902': 35, u'anaphora=\u0939\u092e\u093e\u0930\u0947': 87, 'distance': 116, u'anaphora=\u0939\u092e\u093e\u0930\u0940': 86, u'anaphora=\u0909\u0928\u0915\u093e': 29, u'anaphora=\u0924\u0939\u093e\u0902': 67, u'anaphora=\u091c\u093f\u0938\u0915\u0947': 60, 'antperson=any': 109, u'anaphora=\u091c\u093f\u0938\u0915\u0940': 59, u'anaphora=\u0909\u0938\u0928\u0947': 41, u'anaphora=\u0909\u0928\u0915\u0947': 31, 'anasemprop=h': 91, 'antsemprop=rest': 115, u'anaphora=\u0909\u0928\u0915\u0940': 30, u'anaphora=\u0909\u0938\u0915\u093e': 38, u'anaphora=\u0909\u0928\u0915\u094b': 32, 'antnumber=pl': 102, u'anaphora=\u0935\u0939': 75, u'anaphora=\u091c\u093f\u0928\u0938\u0947': 54, 'anaPRP_type=O': 4, 'anaPRP_type=R': 5, u'anaphora=\u0909\u0938\u0938\u0947': 43, u'anaphora=\u0905\u092a\u0928\u093e': 18, 'anaperson=3h': 15, u'anaphora=\u0909\u0928\u092e\u0947\u0902': 33, u'anaphora=\u0905\u092a\u0928\u0947': 20, u'anaphora=\u0905\u092a\u0928\u0940': 19, u'anaphora=\u0939\u092e\u093e\u0930\u093e': 85, u'anaphora=\u0935\u0947': 80, u'anaphora=\u0935\u094b': 81, u'anaphora=\u092e\u0948\u0902': 73, u'anaphora=\u0909\u0938\u0915\u0947': 40, 'antPOSTag=NNP': 98, u'anaphora=\u0909\u0938\u0915\u0940': 39, 'anaPRP_type=VH': 7, 'anasemprop=any': 90, 'anaperson=2h': 13, 'antperson=none': 110, 'anaperson=none': 17}

            #print vec.vocabulary_
            X=vec.transform(dis)
            #print vec.fit_transform(dis).toarray()
            #print vec.get_feature_names()
            ans=None
            if 'yes' in gnb_loaded.predict(X):
                prd=countTochunkmapping[i]
                if self.chunk.getAttribute('semprop')==prd.getAttribute('semprop'):
                    #print countTochunkmapping[i].name ,countTochunkmapping[i].getAttribute('head')
                    ans=prd
                    break
            else:
                ans=None
                
                
        if ans!=None and self.chunk.isPronoun and self.chunk!=ans:
            self.chunk.PrePronounrefstm=ans.upper
            self.chunk.PrePronounrefnode=ans
            self.chunk.isPronounresolved=True
            
        return self.chunk

    
class anaphora_resolution_main():
    def __init__(self, filePath,preffile,modelpath):
        self.filePath=filePath
        self.pronounsInResolution=[]
        self.pronounResolutionModuleType=None
        self.useShallowFeature=None
        self.useDepFeature=None
        self.usesemprop=None
        self.useNER=None
        self.__pronouns={}
        self.prefFilepath=preffile
	self.modelpath=modelpath
        self=self.parse_preference()
        self.resolution_run()
        
        
    def parse_preference(self):
        #print 'you are in parse preference function'
        preference_file=codecs.open(self.prefFilepath, 'r', 'utf-8')
        pre_data=preference_file.readlines()
        line_count=0
        int_line_count=0
        stm_count=-1
        current_pronoun=None
        j1=0
        line_cc=None
        for i in pre_data:
            if i.strip()!='':
                if i[0]!='#':
                    i_split=i.split()
                    if line_count==0:
                        for j in i_split:
                            if '@'== j.strip():
                                continue
                            if '#' in j.strip():
                                break
                            self.pronounsInResolution.append(j.strip())
                    
                    elif line_count==1:
                        for j in i_split:
                            if '@'== j.strip():
                                continue
                            if '#' in j.strip():
                                break
                            self.pronounResolutionModuleType=j.strip()
                    
                    elif line_count==2:
                        for j in i_split:
                            if '@'== j.strip():
                                continue
                            if '#' in j.strip():
                                break
                            self.useShallowFeature=j.strip()
                    
                    elif line_count==3:
                        for j in i_split:
                            if '@'== j.strip():
                                continue
                            if '#' in j.strip():
                                break
                            self.useDepFeature=j.strip() 
                    
                    elif line_count==4:
                        for j in i_split:
                            if '@'== j.strip():
                                continue
                            if '#' in j.strip():
                                break
                            self.usesemprop=j.strip()
                    
                    elif line_count==5:
                        for j in i_split:
                            if '@'== j.strip():
                                continue
                            if '#' in j.strip():
                                break
                            self.useNER=j.strip()
                    else:
                        j1=0
                        if int_line_count==0:
                            current_pronoun=i_split[1].strip()
                            self.__pronouns[current_pronoun]={}
                        elif int_line_count==1:
                            self.__pronouns[current_pronoun]['PRPRoot']=[]
                            for j in i_split:
                                self.__pronouns[current_pronoun]['PRPRoot'].append(j.strip())
                        elif int_line_count==2:
                            self.__pronouns[current_pronoun]['PRPbackrefstm']=[]
                            self.__pronouns[current_pronoun]['PRPbackrefstm'].append(i_split[1])
                            stm_count=int(i_split[1])
                        if stm_count!=-1:
                            if int_line_count!=2:
                                for j in i_split:
                                    if '@'== j.strip():
                                        continue
                                    if '#' in j.strip():
                                        break
                                    if j1==0:
                                        line_cc=j.strip()
                                        self.__pronouns[current_pronoun][int(line_cc)]=[]
                                    else:
                                        self.__pronouns[current_pronoun][int(line_cc)].append(j.strip())
                                    j1=j1+1
                            stm_count=stm_count-1
                            if stm_count==-1:
                                int_line_count=-1 
                        int_line_count=int_line_count+1
                    line_count=line_count+1
        return self
    
    def resolution_run(self):
        #fileList = ssf.folderWalk(self.filePath)
        fileName=self.filePath
        newFileList = []
        anaphoraDic={}
        #for fileName in fileList :
        if True:
            xFileName = fileName.split('/')[-1]
            if xFileName == 'err.txt' or xFileName.split('.')[-1] in ['comments','bak'] or xFileName[:4] == 'task' :
                gf=0
            else :
                newFileList.append(fileName)
        ana_count=0
        for fileName in newFileList :
            d = ssf.Document(fileName)
            for tree in d.nodeList :
                for chunkNode in tree.nodeList :
                    for node in chunkNode.nodeList :
                        if node.type.strip() == 'PRP':
                            s=chunkNode
                            #print 'PRP FIRST ', node.name , node.lex, node.type , node.morphroot,'\n\n\n'
                            chunkNode.isPronoun=True
                            for i in self.__pronouns:
                                if node.morphroot in self.pronounsInResolution:
                                    if (node.morphroot in self.__pronouns[i]['PRPRoot'] and i=='reflexive'):
                                        #print 'PRONOUN NEXT NEXT Reflexive'
                                        s1=reflexive_resolution(node)
                                        s=s1.RFResolve()
                                        #print '\n\nR ',s.chunk.isPronounresolved
                                    elif node.morphroot in self.__pronouns[i]['PRPRoot'] and i=='firstperson':
                                        #print 'PRONOUN NEXT NEXT FP'
                                        s1=firstPerson_resolution(node)
                                        s=s1.FPResolve()
                                        #print '\n\nFP   ',s.chunk.isPronounresolved
                                    elif node.morphroot in self.__pronouns[i]['PRPRoot'] and i=='locative':
                                        #print 'PRONOUN NEXT NEXT Locative'
                                        s1=locative_resolution(node)
                                        s=s1.LResolve()
                                        #print '\n\n LC ::  ',s.chunk.isPronounresolved
                                    elif node.morphroot in self.__pronouns[i]['PRPRoot'] and i=='secondperson':
                                        #print 'PRONOUN NEXT NEXT SP'
                                        s1=secondPerson_resolution(node)
                                        s=s1.SPResolve()
                                        #print '\n\n  :: SP',s.chunk.isPronounresolved
                                    elif (node.morphroot in self.__pronouns[i]['PRPRoot'] and i=='relative'):
                                        #print 'PRONOUN NEXT NEXT REL'
                                        s1=relative_resolution(node,chunkNode)
                                        s=s1.RResolve()
                                        #print '\n\nRL::   ',s.chunk.isPronounresolved
                                    elif (node.morphroot in self.__pronouns[i]['PRPRoot'] and i=='thirdperson'):
                                        #print 'PRONOUN NEXT NEXT TP'
                                        s1=thirdPerson_resolution(node,chunkNode)
                                        s=s1.TPResolve()
                                        #print 'TP: ',s.isPronounresolved
                            
                            #print '\n in mal if \n'
                            if not s.isPronounresolved:
                                for i in self.__pronouns:
                                        #print 'vandan mujadia'
                                        if node.morphroot in self.__pronouns[i]['PRPRoot'] and i=='firstperson':
                                            s1=ML_base_resolution(node,'FP',self.modelpath)
                                            s=s1.MLResolve()
                                            #break
                                        elif node.morphroot in self.__pronouns[i]['PRPRoot'] and i=='locative':
                                            s1=ML_base_resolution(node,'L',self.modelpath)
                                            s=s1.MLResolve()
                                            #break
                                        elif node.morphroot in self.__pronouns[i]['PRPRoot'] and i=='secondperson':
                                            s1=ML_base_resolution(node,'SP',self.modelpath)
                                            s=s1.MLResolve()
                                            #break
                                        elif node.morphroot in self.__pronouns[i]['PRPRoot'] and i=='relative':
                                            s1=ML_base_resolution(node,'JO',self.modelpath)
                                            s=s1.MLResolve()
                                            #break
                                        elif node.morphroot in self.__pronouns[i]['PRPRoot'] and i=='reflexive':
                                            s1=ML_base_resolution(node,'R',self.modelpath)
                                            s=s1.MLResolve()
                                            #break
                                        elif node.morphroot in self.__pronouns[i]['PRPRoot'] and i=='thirdperson':
                                            s1=ML_base_resolution(node,'VH',self.modelpath)
                                            s=s1.MLResolve()
                                            #break
                                        elif node.morphroot in self.__pronouns[i]['PRPRoot'] and i=='other':
                                            s1=ML_base_resolution(node,'O',self.modelpath)
                                            s=s1.MLResolve()
                                            #break
                                        #print 'PRP FIRST from learning ', node.name , node.lex, node.type , node.morphroot,'\n\n\n'
                                        #if s.chunk.isPronounresolved:
                                            #print 'end ML', s.chunk.isPronounresolved ,s.chunk.PrePronounrefnode.name
                                    
                            if True:
                                    if s.isPronounresolved:
                                        ana_count=ana_count+1
                                        #print '\n',s.upper.name,s.name ,'==',s.PrePronounrefnode.upper.name,s.PrePronounrefnode.name,ana_count
                                        if s.PrePronounrefnode in anaphoraDic:
                                            #print 'yes'
                                            #print  'before ',s.upper.name,s.name ,'==',s.PrePronounrefnode.upper.name,s.PrePronounrefnode.name
                                            temp=s.PrePronounrefnode
                                            flag=True
                                            while flag:
                                                #print 'vandan'
                                                if temp in anaphoraDic:
                                                    temp=anaphoraDic[temp]
                                                else:
                                                    flag=False
                                            self.update_anaphoraInSSF(s, temp, ana_count)
                                            anaphoraDic[temp]=s
                                            #print 'after ',s.upper.name,s.name ,'==',temp.upper.name,temp.name
                                        else:
                                            #print 'no'
                                            self.update_anaphoraInSSF(s, s.PrePronounrefnode, ana_count)
                                            anaphoraDic[s.PrePronounrefnode]=s
                                            
                                        
                            break            
            for tree in d.nodeList :
                	print tree.printSSFValue(allFeat=False)
                            
                                
                                        
    def update_anaphoraInSSF(self,ana,ana_ant,number):
        #print 'vandan'
        #print '\n\n for ana_ant',number,'  \n\n'
        for i in ana_ant.upper.nodeList:
	
            if ana_ant==i:
                #print 'ana_antId    ',i.getAttribute('ana_antId')
                #print i.name, ana_ant.name,number
                i.addAttribute('ana_antId',str(number))
                #print 'ana_antId af   ',i.getAttribute('ana_antId')
                break
                #print i.name, i.getAttribute('head')
        
        #print '\n\n\n for ana-- \n\n'
        for ii in ana.upper.nodeList:
            if ii==ana:
                #print 'anaId      ',i.getAttribute('anaId')
                #print i.name, ana.name,number
                ii.addAttribute('anaId',str(number))
                #print 'anaId af      ',i.getAttribute('anaId')
                break
                #print i.name, i.getAttribute('head')
                #print tree.printSSFValue(allFeat=False)            
                                    
                                            
'resolution main class'
if __name__ == '__main__' :
	
    import imp
    modules=['ssfAPI_minimal','argparse','random','numpy','cPickle','sklearn']
    for module in modules:
       try:
           imp.find_module(module)
       except ImportError:
           print 'Error'
	   print 'you don`t have following dependancy module :', module
	   if module=='sklearn':
		print 'for Ubuntu :'
		print '\t\t ::  sudo apt-get install python-sklearn'
		print 'for fedora :'
		print '\t\t  ::  sudo yum install python-scikit-learn'
		print 'please go throw : '
		print '\t\t\t  http://scikit-learn.org/stable/install.html'		
	   exit()



    import ssfAPI_minimal as ssf
    import random
    import numpy as np
    import cPickle
    from sklearn.feature_extraction import DictVectorizer
    #from sklearn.ensemble import RandomForestClassifier 
    from sklearn import tree
    import argparse
    





    parser = argparse.ArgumentParser()
    parser.add_argument('-i', '--input', '-please specify the input file path')
    parser.add_argument('-p', '--path', '-please specify the anaphoraresolver tool`s path')
    
    args = parser.parse_args()

    if args.input==None:
	print 'For help please use: python anaphora_resolution_main.py -h'
	exit()	
    #if True:
    try:
        #first=anaphora_resolution_main('/home/vandan/workspace/anaphoraResolution/tools/data/vv.txt','preferencefiles/PREFERENCE')
	first=anaphora_resolution_main(args.input,args.path+'/preferencefiles/PREFERENCE',args.path+'/model/anaphora.model')
   
    except:
        #print 'something went wrong please check the SSF file format' 
	
	fileName=args.input
        newFileList = []
        anaphoraDic={}
        #for fileName in fileList :
        if True:
            xFileName = fileName.split('/')[-1]
            if xFileName == 'err.txt' or xFileName.split('.')[-1] in ['comments','bak'] or xFileName[:4] == 'task' :
                gf=0
            else :
                newFileList.append(fileName)
        ana_count=0
        for fileName in newFileList :
            d = ssf.Document(fileName)
            for tree in d.nodeList : 
		print tree.printSSFValue(allFeat=False)          
     
                    
                    
                    
                    
                    
                    
                    
                    
