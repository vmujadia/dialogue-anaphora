import codecs
import re
import sys
import argparse


parser = argparse.ArgumentParser()
parser.add_argument('-i', '--input', '-please specify the input file path')

args = parser.parse_args()

if args.input==None:
	print 'For help please use: python featurecopyanaphora.py -h'
	exit()

f=codecs.open(args.input,'r')
Data=f.readlines()

try:
	line_no=0
	dic_annotated={}
	dic_ann_ana={}
	dic_ann_ana_ant={}

	for i in Data:
		line_no=line_no+1
		searchObj_anaId=re.search(r'.*anaId=(\'|")([0-9]+)(\'|").*',i,re.M|re.I)
		searchObj_ana_antId=re.search(r'.*ana_antId=(\'|")([0-9]+)(\'|").*',i,re.M|re.I)
		if True:
			if searchObj_anaId:
				tms=str(i).split(' ')
		                for i1 in tms:
		                        A1=re.search(r'.*anaId=(\'|")([0-9]+)(\'|").*',str(i1).strip(),re.M|re.I)
		                        if A1:
		                                dic_ann_ana[A1.group(2)]=line_no

			if searchObj_ana_antId:
				tms=str(i).split(' ')	
				for i1 in tms:
					A1=re.search(r'.*ana_antId=(\'|")([0-9]+)(\'|").*',str(i1).strip(),re.M|re.I)
					if A1:
						dic_ann_ana_ant[A1.group(2)]=line_no

	for i in dic_ann_ana:
		dic_annotated[dic_ann_ana.get(i)]=dic_ann_ana_ant.get(i)
	#print dic_annotated
	#print sorted(dic_annotated)
	for i in sorted(dic_annotated):
		st1=i-1
		st2=dic_annotated[i]-1
		replace=''
		head=''
		#print Data[st1]
		#print Data[st2]
		#print '\n\n\n'
		if re.search(r".* af='.*,.*,.*,.*,.*,.*,.*,.*'.*",Data[st2],re.M|re.I):
			vandantmp=Data[st2].split(' ')
			for ig in vandantmp:
				#print ig
				fs2=re.search(r'.*(af=\'.*,.*,.*,.*,.*,.*,.*,.*\')',ig.strip(),re.M|re.I)
				fs3=re.search(r'head=("|\')(.*)("|\')',ig.strip(),re.M|re.I)
				if fs2:
					replace=fs2.group(1)
					replace=replace.replace('af=\'','').split('\'',1)[0]
				if fs3:
					head=fs3.group(2)
					#print head
					head=head.split('\'',1)[0]
			while True:
				se1=re.search(r'.*\)\).*',Data[st1],re.M|re.I)
				if se1:
					break
				else:
					tmp=Data[st1].split(',',1)
					tmp2=tmp[1].split('\'',1)
					
					
					#print replace					
					feature=replace.split(',',1)
					if feature[0].strip()!='':					
						#print tmp[0].strip(), feature[1].strip(), feature[0].strip(), tmp[1].strip()
						final_str=tmp[0]+','+feature[1]+'\' '+'refroot=\''+feature[0]+'\' refhead=\''+head+'\' '+tmp2[1]
						Data[st1]=final_str
						#print final_str
				st1=st1+1
	
	val=False
	for i in Data:
		if re.search(r'.*<Sentence id=.*',i,re.M|re.I):
			val=True		
		if val:	
			print i.rstrip()
	
	
except:
	print 'something went wrong'
	val=False
	for i in Data:
		if re.search(r'.*<Sentence id=.*',i,re.M|re.I):
			val=True		
		if val:	
			print i.rstrip()
	
