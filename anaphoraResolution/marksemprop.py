# -*- coding: utf-8 -*-

'''
import imp
modules=['ssfAPI_minimal','argparse','clientWordnet']
for module in modules:
    try:
        imp.find_module(module)
    except ImportError:
        print 'Error'
	print 'you don`t have following dependancy module :', module
	exit()
'''

import ssfAPI_minimal as ssf
import os
import sys
import codecs
import argparse
#import clientWordnet as cw



parser = argparse.ArgumentParser()
parser.add_argument('-i', '--input', '-please specify the input file path')

args = parser.parse_args()

if args.input==None:
	print 'For help please use: python featurecopyanaphora.py -h'
	exit()


#fileList = ssf.folderWalk('/home/vandan/workspace/anaphoraResolution/tools/data/')
#fileList = ssf.folderWalk('/home/vandan/workspace/anaphoraResolution/test/')
filename=args.input
newFileList = []
if True :
	xFileName = filename
	if xFileName == 'err.txt' or xFileName.split('.')[-1] in ['comments','bak'] or xFileName[:4] == 'task' :
		sd=0
	else :
		newFileList.append(filename)
	prp_list=[u'वह',u'अपने', u'अपना', u'अपनी', u'खुद',u'हम']   


try:
	#conn = cw.wordnetConnection('10.2.8.158',int('50009'))
	
	for fileName in newFileList :
	    d = ssf.Document(fileName)
	    for tree in d.nodeList :
		        for chunkNode in tree.nodeList :
		            if chunkNode.type=='NP':
		                value='rest'
		                for node in chunkNode.nodeList :
		                    temp='6'
				    #print node.lex
				    #temp=conn.getAnimacy(node.lex.encode('utf-8'))
		                    if int(temp)==1:
		                        value='h'
		                    if node.getAttribute('enamex_type')=='person':
		                        value='h'
				    if node.getAttribute('esubtype')=='person':
		                        value='h'	
		                    if node.morphroot in prp_list:
		                        value='h'    
		                chunkNode.addAttribute('semprop',value)
				#print chunkNode.printSSFValue(allFeat=False)
		        #tree.updateAttributes()
		        print tree.printSSFValue(allFeat=False)
	#conn.close()
except:
	for fileName in newFileList :
	    d = ssf.Document(fileName)
	    for tree in d.nodeList :
		print tree.printSSFValue(allFeat=False)
