#!/usr/bin/python
# -*- coding: utf-8 -*-
import os
import sys
import codecs
import re
import locale
from os import listdir
from os.path import isfile,join
from ssfAPI_minimal import *

def generate_filelist(path):
   return [path + f for f in listdir(path) if isfile(join(path,f))]

def print_score(inter_score,filename,scr_type):
    orig_stdout = sys.stdout;
    if scr_type=="v":
        typ = "_vb";
    elif scr_type=="n":
        typ = "_nn";
    if isfile("interscore_" + filename.split("/")[-1] + typ):
        f = codecs.open("interscore_" + filename.split("/")[-1] + typ,'a',encoding='utf-8');
    else:
        f = codecs.open("interscore_" + filename.split("/")[-1] + typ,'w',encoding='utf-8');
    sys.stdout = f;
    utt_num = len(inter_score)+1;
    print "*****utterance" + str(utt_num) + "********"
    for curr_utt in range(0,len(inter_score)):
        print str(utt_num) + " vs " + str(curr_utt+1),
        print "\t" + str(inter_score[curr_utt]);
    sys.stdout = orig_stdout

def print_score_intra(intra_score,filename,scr_type,utt_num,utt_printed):
    new_file = 0;
    orig_stdout = sys.stdout;
    if scr_type=="v":
        typ = "_vb";
    elif scr_type=="n":
        typ = "_nn";
    if isfile("intrascore_" + filename.split("/")[-1] + typ):
        f = codecs.open("intrascore_" + filename.split("/")[-1] + typ,'a',encoding='utf-8');
    else:
        f = codecs.open("intrascore_" + filename.split("/")[-1] + typ,'w',encoding='utf-8');
    sys.stdout = f;
    
    if utt_printed==1:
        print "***********************utterance" + str(utt_num) + "*********************************"
    
    sen_num = len(intra_score)+1;
    print "**********sentence" + str(sen_num) + "***********"
    for curr_sen in range(0,len(intra_score)):
        print str(sen_num) + " vs " + str(curr_sen+1),
        print "\t" + str(intra_score[curr_sen]);
    sys.stdout = orig_stdout



def cal_score(all_list, total_tags_utt):
    inter_score = [0.0 for x in xrange(len(all_list)-1)]
    for tag in all_list[-1]:
        freq = all_list[-1][tag];  #frequency of verb
        total_tags = total_tags_utt[-1];
        for utt in range(0,len(all_list)-1): #checking verb matching for all the previous utterances
            for t in all_list[utt]:
                if tag == t:
                    freq2 = all_list[utt][t];
                    total_tags2 = total_tags_utt[utt];
                    inter_score[utt] += (float((float(freq) + float(freq2))/(float(total_tags) + float(total_tags2))));
                    inter_score[utt] = float(float(inter_score[utt])/float(((len(all_list)-1) - utt)));
    return inter_score;


def check_verb(node):
#    print node.morphroot,
 #   print node.morphPOS
    if node.morphPOS == "v" and node.morphroot.encode("utf-8")!="है":
        return 1;
    else:
        return 0;

def check_noun(node):
#    print node.morphroot,
 #   print node.morphPOS
    if node.morphPOS == "n":
        return 1;
    else:
        return 0;

def print_tag_utt(tag_list,tag_type,filename):
    orig_stdout = sys.stdout;
    if tag_type=="v":
        typ = "inter_verbs_";
    elif tag_type=="n":
        typ = "inter_nouns_";
    if isfile(typ + filename.split("/")[-1]):
        f = codecs.open(typ + filename.split("/")[-1],'a',encoding='utf-8');
    else:
        f = codecs.open(typ + filename.split("/")[-1],'w',encoding='utf-8');
    sys.stdout = f;
    for dic in tag_list:
        for wrd in dic:
            print wrd + " " + str(dic[wrd]),
        print ""
    sys.stdout = orig_stdout


def print_tag_sen(tag_list,tag_type,filename):
    orig_stdout = sys.stdout;
    if tag_type=="v":
        typ = "intra_verbs_";
    elif tag_type=="n":
        typ = "intra_nouns_";
    if isfile(typ + filename.split("/")[-1]):
        f = codecs.open(typ + filename.split("/")[-1],'a',encoding='utf-8');
    else:
        f = codecs.open(typ + filename.split("/")[-1],'w',encoding='utf-8');
    sys.stdout = f;
    print "************new utterance**************"
    for dic in tag_list:
        for wrd in dic:
            print wrd + " " + str(dic[wrd]),
        print ""
    sys.stdout = orig_stdout



def intra_utterance(fileList):
    for fileName in fileList:
        num_utterances = 0;
        d = Document(fileName)
        utt_printed = 0; #keeping record of whether the utt number printed or not
        utt_change = 0; #hyphen present before the next chunk
        for tree in d.nodeList: #new sentence
            if num_utterances > 0:
                total_nn_sen.append(nn_curr_sen)
                nn_all_list.append(nn_freq_dic);
                #print_tag_sen(nn_all_list,"n",fileName);
                nn_sen_score = cal_score(nn_all_list,total_nn_sen); #calculate the score for the previous utterance
                if utt_printed!=num_utterances:
                    utt_printed = num_utterances;
                    print_score_intra(nn_sen_score,fileName,"n",num_utterances,1);
                else:
                    print_score_intra(nn_sen_score,fileName,"n",num_utterances,0);

                total_vb_sen.append(vb_curr_sen)
                vb_all_list.append(vb_freq_dic);
                #print_tag_sen(nn_all_list,"n",fileName);
                vb_sen_score = cal_score(vb_all_list,total_vb_sen); #calculate the score for the previous utterance
                if utt_printed!=num_utterances:
                    utt_printed = num_utterances;
                    print_score_intra(vb_sen_score,fileName,"v",num_utterances,1);
                else:
                    print_score_intra(vb_sen_score,fileName,"v",num_utterances,0);


            nn_freq_dic = {} #new sentence starts, so emptying the dictionary
            nn_curr_sen = 0;
                
            vb_curr_sen = 0;
            vb_freq_dic = {} #new sentence starts, so emptying the dictionary
            for chunkNode in tree.nodeList:
                speaker = chunkNode.getAttribute('speaker')  #getting the speaker name
                if(speaker): #new utterance
                    if num_utterances > 0:
                        """
                        if utt_change == 1:
                            utt_change = 0;
                            for nn in nn_prev_chunk:
                                if nn in nn_
                        """
                        if utt_change==0:
                            total_nn_sen.append(nn_curr_sen)
                            nn_all_list.append(nn_freq_dic);
                        
                        print_tag_sen(nn_all_list,"n",fileName);
                        nn_sen_score = cal_score(nn_all_list,total_nn_sen); #calculate the score for the previous utterance
                        print_score_intra(nn_sen_score,fileName,"n",num_utterances,0);

                        if utt_change==0:
                            total_vb_sen.append(vb_curr_sen)
                            vb_all_list.append(vb_freq_dic);
                        print_tag_sen(vb_all_list,"v",fileName);
                        vb_sen_score = cal_score(vb_all_list,total_vb_sen); #calculate the score for the previous utterance
                        print_score_intra(vb_sen_score,fileName,"v",num_utterances,0);

                        utt_change=0;


                    nn_all_list = [];
                    nn_freq_dic = {};
                    total_nn_sen = [];  #total nn per sentence as each item in list
                    nn_curr_sen = 0;

                    vb_all_list = [];
                    vb_freq_dic = {};
                    total_vb_sen = [];  #total nn per sentence as each item in list
                    vb_curr_sen = 0;

                    num_utterances += 1;
                
#                previous_chunk_len = chunk_len
 #               chunk_len = len(chunkNode.nodeList);
                

#                nn_prev_chunk = nn_curr_chunk;
 #               vb_prev_chunk = vb_curr_chunk;
  #              nn_curr_chunk = {}
   #             vb_curr_chunk = {}
                
                for node in chunkNode.nodeList:
                    if len(chunkNode.nodeList)==1:
                        if node.lex == "-":
                            utt_change = 1;
                            break;
                    verb_flag = check_verb(node);
                    if verb_flag==1: #if it is a verb add to the dictionary acc. to its previous count
                        vb_curr_sen += 1;
                        if node.morphroot not in vb_freq_dic:
                            vb_freq_dic[node.morphroot] = 1;
                        else:
                            vb_freq_dic[node.morphroot] += 1;
                        """
                        if node.morphroot not in vb_previous_chunk:
                            vb_curr_chunk[node.morphroot] = 1;
                        else:
                            vb_curr_chunk[node.morphroot] += 1;
                        """

                    noun_flag = check_noun(node);
                    if noun_flag==1: #if it is a verb add to the dictionary acc. to its previous count
                        nn_curr_sen += 1;
                        if node.morphroot not in nn_freq_dic:
                            nn_freq_dic[node.morphroot] = 1;
                        else:
                            nn_freq_dic[node.morphroot] += 1;
                        """
                        if node.morphroot not in nn_previous_chunk:
                            nn_curr_chunk[node.morphroot] = 1;
                        else:
                            nn_curr_chunk[node.morphroot] += 1;
                        """


        #print_tag_utt(all_list,"v",fileName);




def split_by_utterance(fileList):
    for fileName in fileList :
        #speaker = ''
        all_list = [] #will contain  dictionary of verbs for each utterance
        freq_dic = {} #dictionary of frequency of each word in the current utterance
        total_verbs_utt = [] #contain list of total verbs in every utterance
        verbs_curr_utt = 0; #total verbs in current utterance
        nn_all_list = []
        nn_freq_dic={}
        total_nn_utt = []
        nn_curr_utt = 0;
        utt_change = 0; 
        num_utterances=0;
        d = Document(fileName)
        for tree in d.nodeList :

            nn_curr_chunk = {}
            vb_curr_chunk = {}
            chunk_len = 0;
            for chunkNode in tree.nodeList :
         #       if speaker:
          #          previous_speaker = speaker
                speaker = chunkNode.getAttribute('speaker')  #gettin the speaker name
                if(speaker): #if speaker is mentioned for the current chunk , a new utterance starts
                    curr_utt_node = chunkNode;
                    if num_utterances>0: #if it is not the first utterance

                        if utt_change==1:
                            utt_change=0;
                            for vb in vb_previous_chunk:
                                if vb in freq_dic:
                                    verbs_curr_utt -= vb_previous_chunk[vb];
                                    freq_dic[vb] -= vb_previous_chunk[vb];
                                    if freq_dic[vb]==0:
                                        del freq_dic[vb];
                            for nn in nn_previous_chunk:
                                if nn in nn_freq_dic:
                                    nn_curr_utt -= nn_previous_chunk[nn];
                                    nn_freq_dic[nn] -= nn_previous_chunk[nn];
                                    if nn_freq_dic[nn]==0:
                                        del nn_freq_dic[nn];

                        total_verbs_utt.append(verbs_curr_utt);
                        verbs_curr_utt=0;
                        all_list.append(freq_dic); #storing the previous utterance dict
                        freq_dic={} #dict for new utterance
                        vb_utt_score = cal_score(all_list,total_verbs_utt); #calculate the score for the previous utterance
                        print_score(vb_utt_score,fileName,"v");
                        total_nn_utt.append(nn_curr_utt);
                        nn_curr_utt = 0;
                        nn_all_list.append(nn_freq_dic); #storing the previous utterance dict
                        nn_freq_dic={} #dict for new utterance
                        nn_utt_score = cal_score(nn_all_list,total_nn_utt); #calculate the score for the previous utterance 
                        print_score(nn_utt_score,fileName,"n");



                        #change = check_topic_changing(utt_score,speaker)
                        #if change==1:

                            #topic_id += 1;
                        #   add_topic(topic_id,curr_utt_node);
                    #if num_utterances==0:
                        #topic_id=0;
                        #add_topic(topic_id);
                        #last_topic
                    num_utterances += 1;

                previous_chunk_len = chunk_len
                chunk_len = len(chunkNode.nodeList);

                nn_previous_chunk = nn_curr_chunk;
                vb_previous_chunk = vb_curr_chunk;
                nn_curr_chunk = {}
                vb_curr_chunk = {}


                for node in chunkNode.nodeList:
                    if len(chunkNode.nodeList)==1:
                        if node.lex == "-":
                            utt_change = 1;
                            break;
                    verb_flag = check_verb(node);
                    if verb_flag==1: #if it is a verb add to the dictionary acc. to its previous count
                        verbs_curr_utt += 1;
                        if node.morphroot not in freq_dic:
                            freq_dic[node.morphroot] = 1;
                        else:
                            freq_dic[node.morphroot] += 1;
                        if node.morphroot not in vb_curr_chunk:
                            vb_curr_chunk[node.morphroot] = 1;
                        else:
                            vb_curr_chunk[node.morphroot] += 1;

                    noun_flag = check_noun(node);
                    if noun_flag==1: #if it is a verb add to the dictionary acc. to its previous count
                        nn_curr_utt += 1;
                        if node.morphroot not in nn_freq_dic:
                            nn_freq_dic[node.morphroot] = 1;
                        else:
                            nn_freq_dic[node.morphroot] += 1;
                        if node.morphroot not in nn_curr_chunk:
                            nn_curr_chunk[node.morphroot] = 1;
                        else:
                            nn_curr_chunk[node.morphroot] += 1;
                        

        print_tag_utt(all_list,"v",fileName);
        print_tag_utt(nn_all_list,"n",fileName);


def deletefiles():
    os.system("rm -f inter*");
    os.system("rm -f intra*");

if __name__ == '__main__' :
    #fileList = generate_filelist('/home/darshan/Dropbox/Anaphora/from_ssf/speaker_conversation/')
    path = sys.argv[1]
    fileList = generate_filelist(path)
    deletefiles();
    orig_stdout = sys.stdout; #saving the original stdout
    split_by_utterance(fileList);
    intra_utterance(fileList);
    sys.stdout = orig_stdout
