# -*- coding: utf-8 -*-
import re
import ssfAPI_minimal as ssf
import codecs
import argparse


class markSpeakerSsf_main():
	def __init__(self, fileName):
		#print 'yes you are in markSpeaker class'
		self.fileName=fileName
		self.possiblespeakerList=['narrator']
		self.speaker=None
		self.verbList=[u'दोहरा',u'चाह',u'समझ',u'बता',u'बुला',u'कह']
		self.markSpeakerSsf_run()

	def findk1forverb(self,node):
		for b in node.upper.upper.nodeList:
			#print b.name,'vandan'
			#print b.parent, b.parentRelation
			for c in b.nodeList:
				gf=0
				if b.parentRelation=='k1':
					self.speaker=c.lex
					#print c.lex



	def markSpeakerSsf_run(self):
		#print 'in markspeaker run'
		fileName=self.fileName
		newFileList = []
		anaphoraDic={}
		#for fileName in fileList :
		if True:
		    xFileName = fileName.split('/')[-1]
		    if xFileName == 'err.txt' or xFileName.split('.')[-1] in ['comments','bak'] or xFileName[:4] == 'task' :
		        gf=0
		    else :
		        newFileList.append(fileName)
		flag=False
		sprdrel=False
		sprdrel1=False
		for fileName in newFileList :
		    d = ssf.Document(fileName)
		    #print '<Sentence id="'+d.name+'">'
		    for tree in d.nodeList :
			#print tree.name
			#print self.speaker
			speakercount=0
			tokencount=0
			#print '<Sentence id="'+tree.name+'">'
		        for chunkNode in tree.nodeList :
			    #print chunkNode.parent
			    if sprdrel or sprdrel1:
				    speakercount=speakercount+1
				    if speakercount==2:
				    	chunkNode.addAttribute('speaker',self.speaker)
					if self.speaker not in self.possiblespeakerList:
						self.possiblespeakerList.append(self.speaker)
				    	#print getAttribute('speaker')
				    	sprdrel=False

			    if chunkNode.parentRelation=='spr':
				    sprdrel=True
				    #print 'in true'
			
		            for node in chunkNode.nodeList:
				    #tokencount=tokencount+1
				    #print str(tokencount)+'\t'+node.lex+'\t'+'unk'

				    #print node.type, node.name
				    if node.type=='NNP' and sprdrel:
					    self.speaker=node.lex
					    #print node.lex

			#	    if node.morphroot in self.verbList:
					    #print '\n\n'
					    #print node.lex
		#			    self.findk1forverb(node)
		#			    sprdrel1=True
			
			#print tree.name
			#print self.speaker
					    
			'''
				    if node.lex=='-':
					    #print node.lex
					    flag=True
				    if flag:
					    print node.lex
			'''
		   	#print '</Sentence>'

			print tree.printSSFValue(allFeat=False)
			#print self.possiblespeakerList

		#for ii in self.possiblespeakerList:
			#print ii





'resolution main class'
if __name__ == '__main__' :
    parser = argparse.ArgumentParser()
    parser.add_argument('-i', '--input', '-please specify the input file path')
    parser.add_argument('-p', '--path', '-please specify the anaphoraresolver tool`s path')
    args = parser.parse_args()
    if args.input==None:
        print 'For help please use: python anaphora_resolution_main.py -h'
        exit()
    #try:
    if True:
        first=markSpeakerSsf_main(args.input)
    #except:
        #print 'something went wrong please check the SSF file format'
